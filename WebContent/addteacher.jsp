<%
String metatitle ="Add Teacher";
%>
<%@ include file="includes/header.jsp" %>
<div class="container">
	<div class="col-sm-12 top1">
		<div class="col-sm-3"></div>
		<div class="col-sm-9">
			<%@ include file="includes/teacher-form.jsp" %>
		</div>  
	</div>  
</div>
<%@ include file="includes/footer.jsp" %>