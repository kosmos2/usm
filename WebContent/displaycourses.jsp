<%! 
	String metatitle ="View Courses";
%>
<%@ include file="includes/header.jsp" %>
<div class="container">
	<div class="col-sm-12 top"></div>
	<div class="col-sm-12">
		<%@ include file="includes/table-courses.jsp" %> 
	</div>  
</div>  
<%@ include file="includes/footer.jsp" %>