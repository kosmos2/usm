<%
	String metatitle ="Assign Student to a Course";
%>
<%@ include file="includes/header.jsp" %>
<div class="container">
	<div class="col-sm-12 top1"></div>
	<div class="col-sm-3"></div>
	<div class="col-sm-9">
		<%@ include file="includes/studentcourse-form.jsp" %>
	</div>  
</div>  
<%@ include file="includes/footer.jsp" %>