// JavaScript Document

$(document).ready(function(){
	$("#addstudent p.label-danger").fadeOut();
	$("#addteacher p.label-danger").fadeOut();
	$("#addparent p.label-danger").fadeOut();
	$("#addemergency p.label-danger").fadeOut();
	$("#addcourse p.label-danger").fadeOut();
	$("#addnewteachercourse p.label-danger").fadeOut();
	$("#addnewteacheravail p.label-danger").fadeOut();
	$("#addnewstuteachavail p.label-danger").fadeOut();
	$("#addnewstudentcourse p.label-danger").fadeOut();
	$("#addnewstudentdaycourse p.label-danger").fadeOut();
    $('#myTable').DataTable({dom: 'T<"clear">lfrtip',});
    
	$(".deleterow").on("click", function(){
		var id=$(this).find("a").data("id");
		$.get("ajax/delete_"+$(this).find("a").data("url")+".jsp","RECORDID="+id,function(msg){});
		var $killrow = $(this).parent('tr');
		$killrow.addClass("danger");
		$killrow.fadeOut(2000, function(){
		$(this).remove();
});


});

$(".addnewrow").on("click", function(){
	window.location.href="addstudent.jsp";
});

$(".addnewrow1").on("click", function(){
	window.location.href="addparent.jsp";
});

$(".addnewrow2").on("click", function(){
	window.location.href="addteacher.jsp";
});
$(".addnewrow3").on("click", function(){
	window.location.href="addemergency.jsp";
});
$(".addnewteachercourse").on("click", function(){
	window.location.href="addnewteachercourse.jsp";
});
$(".addnewteacheravail").on("click", function(){
	window.location.href="addnewteacheravail.jsp";
});
$(".addnewstuteachavail").on("click", function(){
	window.location.href="addnewstuteachavail.jsp";
});
$(".addnewstudentcourse").on("click", function(){
	window.location.href="addnewstudentcourse.jsp";
});
$(".addnewcourse").on("click", function(){
	window.location.href="addnewcourse.jsp";
});
$("#addstudent").ajaxForm
({
	beforeSubmit:function()
	{
		var email4 =/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	    var email3=$("#EMAIL").val();
	    
		var dob4 =/^([0-9]{2})-([0-9]{2})-([0-9]{4})$/;
	    var dob3=$("#DOB").val();	    
	    
		$("#addstudent p.label-danger").fadeOut();
		if($("#FIRSTNAME").val().length < 1 || $("#HOMEPHONE").val().length > 40)
		{
			$(".FIRSTNAME").fadeIn();
			return false;
		}
		else if($("#SURNAME").val().length < 1 || $("#SURNAME").val().length > 40)
		{
			$(".SURNAME").fadeIn();
			return false;
		}
		else if($("#STREET").val().length < 1 || $("#STREET").val().length > 140)
		{
			$(".STREET").fadeIn();
			return false;
		}
		else if($("#SUBURB").val().length < 1 || $("#SUBURB").val().length > 40)
		{
			$(".SUBURB").fadeIn();
			return false;
		}
		else if(!$.isNumeric($("#POSTCODE").val()) || $("#POSTCODE").val().length < 1)
		{
			$(".POSTCODE").fadeIn();
			return false;
		}
		else if($("#STATE").val().length < 1 || $("#STATE").val().length > 3)
		{
			$(".STATE").fadeIn();
			return false;
		}
		
		else if(!email4.test(email3))
		{
            $(".EMAIL").fadeIn();
			return false;
        }

	    else if(!dob4.test(dob3))
		{
			$(".DOB").fadeIn();
			return false;
		}
		
		else if($("#HOMEPHONE").val().length < 1 || $("#HOMEPHONE").val().length > 10 )
		{
			$(".HOMEPHONE").fadeIn();
			return false;
		}
		else if($("#MOBILEPHONE").val().length < 1 || $("#MOBILEPHONE").val().length > 10)
		{
			$(".MOBILEPHONE").fadeIn();
			return false;
		}
		else
		{
		
		$(".default_div").fadeOut();
		$(".ajax").fadeIn();
		
		return true;
		}
	},
	success:function(msg)
	{
		$("#status").html(msg);
		
		setTimeout(function()
		{
			
			$("#beforeresult").fadeOut();
			$(".afterresult").fadeIn();
			
		},2000)
	}
})


$("#addparent").ajaxForm
({
	beforeSubmit:function()
	{
		var email4 =/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	    var email3=$("#EMAIL").val();
		$("#addparent p.label-danger").fadeOut();
		
		if($("#FIRSTNAME").val().length < 1 || $("#FIRSTNAME").val().length > 40)
		{
			$(".FIRSTNAME").fadeIn();
			return false;
		}
		else if($("#SURNAME").val().length < 1 || $("#SURNAME").val().length > 40)
		{
			$(".SURNAME").fadeIn();
			return false;
		}
		
		
		
		else if(!email4.test(email3)){
             $(".EMAIL").fadeIn();
			return false;
          
        }
		
		else if($("#HOMEPHONE").val().length < 1 || $("#HOMEPHONE").val().length > 10 )
		{
			$(".HOMEPHONE").fadeIn();
			return false;
		}
		else if($("#MOBILEPHONE").val().length < 1 || $("#MOBILEPHONE").val().length > 10)
		{
			$(".MOBILEPHONE").fadeIn();
			return false;
		}
		else if($("#OCCUPATION").val().length < 1 || $("#OCCUPATION").val().length > 10)
		{
			$(".OCCUPATION").fadeIn();
			return false;
		}
		else
		{
		
		$(".default_div").fadeOut();
		$(".ajax").fadeIn();
		
		return true;
		}
	},
	success:function(msg)
	{
		$("#status").html(msg);
		
		setTimeout(function()
		{
			
			$("#beforeresult").fadeOut();
			$(".afterresult").fadeIn();
			
		},2000)
	}
})


$("#addemergency").ajaxForm
({
	beforeSubmit:function()
	{
		
		var email4 =/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
	    var email3=$("#EMAIL").val();
		$("#addemergency p.label-danger").fadeOut();
		
		if($("#FIRSTNAME").val().length < 1 || $("#FIRSTNAME").val().length > 40)
		{
			$(".FIRSTNAME").fadeIn();
			return false;
		}
		else if($("#SURNAME").val().length < 1 || $("#SURNAME").val().length > 40)
		{
			$(".SURNAME").fadeIn();
			return false;
		}
		
		
		
		else if(!email4.test(email3)){
             $(".EMAIL").fadeIn();
			return false;
          
        }
		
		else if($("#HOMEPHONE").val().length < 1 || $("#HOMEPHONE").val().length > 10 )
		{
			$(".HOMEPHONE").fadeIn();
			return false;
		}
		else if($("#MOBILEPHONE").val().length < 1 || $("#MOBILEPHONE").val().length > 10)
		{
			$(".MOBILEPHONE").fadeIn();
			return false;
		}
		else if($("#RELATIONSHIP").val().length < 1 || $("#RELATIONSHIP").val().length > 10)
		{
			$(".RELATIONSHIP").fadeIn();
			return false;
		}
		else
		{
		
		$(".default_div").fadeOut();
		$(".ajax").fadeIn();
		
		return true;
		}
	},
	success:function(msg)
	{
		$("#status").html(msg);
		
		setTimeout(function()
		{
			
			$("#beforeresult").fadeOut();
			$(".afterresult").fadeIn();
			
		},2000)
	}
})


$("#addteacher").ajaxForm
({
	beforeSubmit:function()
	{

		$("#addteacher p.label-danger").fadeOut();
		if($("#FIRSTNAME").val().length < 1 || $("#FIRSTNAME").val().length > 40)
		{
			$(".FIRSTNAME").fadeIn();
			return false;
		}
		else if($("#SURNAME").val().length < 1 || $("#SURNAME").val().length > 40)
		{
			$(".SURNAME").fadeIn();
			return false;
		}
		else
		{
			$(".default_div").fadeOut();
			$(".ajax").fadeIn();
			return true;
		}
	},
	success:function(msg)
	{
		$("#status").html(msg);
		
		setTimeout(function()
		{
			
			$("#beforeresult").fadeOut();
			$(".afterresult").fadeIn();
			
		},2000)
	}
})

$("#addnewteachercourse").ajaxForm
({
	beforeSubmit:function()
	{
		$("#addnewteachercourse p.label-danger").fadeOut();
		$(".default_div").fadeOut();
		$(".ajax").fadeIn();
	},
	success:function(msg)
	{
		$("#status").html(msg);
		
		setTimeout(function()
		{
			
			$("#beforeresult").fadeOut();
			$(".afterresult").fadeIn();
			
		},2000)
	}
})

$("#addnewteacheravail").ajaxForm
({
	beforeSubmit:function()
	{
		$("#addnewteacheravail p.label-danger").fadeOut();
		$(".default_div").fadeOut();
		$(".ajax").fadeIn();
	},
	success:function(msg)
	{
		$("#status").html(msg);
		
		setTimeout(function()
		{
			
			$("#beforeresult").fadeOut();
			$(".afterresult").fadeIn();
			
		},2000)
	}
})

$("#addnewstuteachavail").ajaxForm
({
	beforeSubmit:function()
	{
		$("#addnewstuteachavail p.label-danger").fadeOut();
		$(".default_div").fadeOut();
		$(".ajax").fadeIn();
	},
	success:function(msg)
	{
		$("#status").html(msg);
		
		setTimeout(function()
		{
			
			$("#beforeresult").fadeOut();
			$(".afterresult").fadeIn();
			
		},2000)
	}
})

$("#addnewstudentcourse").ajaxForm
({
	beforeSubmit:function()
	{
		$("#addnewstudentcourse p.label-danger").fadeOut();
		$(".default_div").fadeOut();
		$(".ajax").fadeIn();
	},
	success:function(msg)
	{
		$("#status").html(msg);
		
		setTimeout(function()
		{
			
			$("#beforeresult").fadeOut();
			$(".afterresult").fadeIn();
			
		},2000)
	}
})

$("#addcourse").ajaxForm
({
	beforeSubmit:function()
	{
		$("#addcourse p.label-danger").fadeOut();
		$(".default_div").fadeOut();
		$(".ajax").fadeIn();
		return true;
	},
	success:function(msg)
	{
		$("#status").html(msg);
		
		setTimeout(function()
		{
			
			$("#beforeresult").fadeOut();
			$(".afterresult").fadeIn();
			
		},2000)
	}
})


});

function addbar()
{
	var vv=parseInt($(".progress-bar").innerWidth())+1;
	$(".progress-bar").width(vv+"%").text(vv+"%");
}

