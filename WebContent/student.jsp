<%! 
	String metatitle ="Students Profile";
%>
<%@ include file="includes/header.jsp" %>
<%@ include file="includes/top-row.jsp" %> 
<div class="container">
		<div class="col-sm-2">
  			<%@ include file="includes/right-tab.jsp" %>
  		</div>
		<div class="col-sm-4">
			<%@ include file="includes/school-information.jsp" %>
		</div>
		<div class="col-sm-6">
			<%@ include file="includes/news.jsp" %>
		</div>
	<!--  <div class="col-sm-12 top1"></div> -->
</div>
<%@ include file="includes/footer.jsp" %>