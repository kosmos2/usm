<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.* " %>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%
	String  tid = request.getParameter("TEACHERID");

	String tt="";
	String myInsertQuery="";
	String myDeleteQuery="";
	String urll="";
	String lab="";
	boolean isDeleted = false;

	tt="New Record Inserted Successfully";
	myDeleteQuery = "DELETE FROM \"TEACHERAVAILABILITY\" where  \"TEACHERID\"='"+tid+"';";
	myInsertQuery = "INSERT INTO \"TEACHERAVAILABILITY\"( \"TEACHERID\", \"DAY\", \"TIME\") VALUES (?, ?,?);";
	urll="teacher.jsp";
	lab="View Teacher";

	try {
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/USM";
		String username = "postgres";
		String password = "postgres";
	
		String generatedColumns[] = {"TEACHERAVAILABILITYID"};
		Connection myConnection = null;
		PreparedStatement myPreparedStatement = null;
		ResultSet myResultSet = null;
		Class.forName(driver).newInstance();
		myConnection = DriverManager.getConnection(url,username,password);

		myPreparedStatement = myConnection.prepareStatement(myDeleteQuery);
		myPreparedStatement.executeUpdate();

		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements())
		{
			String paramName = parameterNames.nextElement();
			if (paramName.startsWith("MON") || paramName.startsWith("TUE") || paramName.startsWith("WED") || paramName.startsWith("THU") ||
					paramName.startsWith("FRI") || paramName.startsWith("SAT") || paramName.startsWith("SUN")) 
			{
				String[] paramValues = request.getParameterValues(paramName);
				for(int i=0; i<paramValues.length; i++)
				{
//old				String paramValue = paramValues[0];
//old				String[] parts = paramName.split("-");
//old				String part1 = parts[0]; // day

					myPreparedStatement = myConnection.prepareStatement(myInsertQuery,generatedColumns);
				
					myPreparedStatement.setInt(1, Integer.parseInt(tid));
					myPreparedStatement.setString(2, paramName);
					myPreparedStatement.setString(3, paramValues[i]);
//old				myPreparedStatement.setString(2, part1);
//old				myPreparedStatement.setString(3, paramValue);
			
					myPreparedStatement.executeUpdate();
				}
			}
		}		
		myPreparedStatement.close();
		myConnection.close();
%>
<h2 id="beforeresult">Your Application is Processing...</h2>

<h3 style="display:none" class="afterresult"><%=tt%></h3>

<div class="col-sm-offset-4 col-sm-10 afterresult">
	<a href="<%=urll%>?TEACHERID=<%=tid%>" class="white" style="color:white"><button type="button" class="btn btn-primary btn-lg "><%=lab%></button></a>
</div>
<%
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>