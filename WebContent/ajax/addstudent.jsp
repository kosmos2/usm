<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.* " %>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%
    String  firstname = request.getParameter("FIRSTNAME");
	String  surname = request.getParameter("SURNAME");
	String  street = request.getParameter("STREET");
	String  suburb = request.getParameter("SUBURB");
    String  state = request.getParameter("STATE");
	String  email = request.getParameter("EMAIL");
	String  homephone = request.getParameter("HOMEPHONE");
	String  workphone = request.getParameter("WORKPHONE");
	String  mobilephone = request.getParameter("MOBILEPHONE");
	String  dob = request.getParameter("DOB");
	String  postcode = request.getParameter("POSTCODE");
	String  middlename = request.getParameter("MIDDLENAME");
	int aInt = Integer.parseInt(postcode);
	String  isgroupOK = request.getParameter("ISGROUPOK");
	boolean groupOK = false;

	if (isgroupOK.equals("true"))
		groupOK=true;

	String tt="";
	String id = "";
	String myQuery="";
	String urll="";
	String lab="";
	if(request.getParameter("STUDENTID")!=null)
   	{
   		id = request.getParameter("STUDENTID");
   	}
	if(id =="")
	{
		tt="New Students Record Inserted Successfully";
		myQuery = "INSERT INTO \"STUDENTS\"(\"FIRSTNAME\", \"SURNAME\", \"STREET\", \"SUBURB\", \"STATE\", \"EMAIL\", \"HOMEPHONE\", \"WORKPHONE\", \"MOBILEPHONE\", \"DOB\", \"POSTCODE\", \"ISGROUPOK\", \"MIDDLENAME\") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		urll="addparent.jsp";
		lab="Add Parents";
	}
	else
	{
		tt="Students Record Updated Successfully";
		myQuery = "UPDATE  \"STUDENTS\" set \"FIRSTNAME\"=?, \"SURNAME\"=?, \"STREET\"=?, \"SUBURB\"=?, \"STATE\"=?, \"EMAIL\"=?,\"HOMEPHONE\"=?, \"WORKPHONE\"=?, \"MOBILEPHONE\"=?,\"DOB\"=?,\"POSTCODE\"=?,\"ISGROUPOK\"=?,\"MIDDLENAME\"=? WHERE \"STUDENTID\"=?";
		urll="student.jsp";
		lab="View Student";
	}
%>

<%
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";

	String generatedColumns[] = {"STUDENTID"};
	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	myPreparedStatement = myConnection.prepareStatement(myQuery,generatedColumns);

	myPreparedStatement.setString(1, firstname);
	myPreparedStatement.setString(2, surname);
	myPreparedStatement.setString(3, street);
	myPreparedStatement.setString(4, suburb);
	myPreparedStatement.setString(5, state);
	myPreparedStatement.setString(6, email);
	myPreparedStatement.setString(7, homephone);
	myPreparedStatement.setString(8, workphone);
	myPreparedStatement.setString(9, mobilephone);
	//myPreparedStatement.setDate(10,java.sql.Date.valueOf(dob));
	myPreparedStatement.setString(10, dob);
	myPreparedStatement.setInt(11, aInt);
	myPreparedStatement.setBoolean(12, groupOK);
	myPreparedStatement.setString(13, middlename);
	if(id !="")
	{
		int aIntt = Integer.parseInt(id);
		myPreparedStatement.setInt(14, aIntt);
	}
	myPreparedStatement.executeUpdate();
	if(id=="")
	{
    	ResultSet rs = myPreparedStatement.getGeneratedKeys();
    	if(rs.next() && id=="")
		{
	 		long productId = rs.getLong(1);
     		id=Long.toString(productId);
		}
	}
	myPreparedStatement.close();
	myConnection.close();
%>
<h2 id="beforeresult">Your Application is Processing...</h2>

<h3 style="display:none" class="afterresult"><%=tt%></h3>

<div class="col-sm-offset-4 col-sm-10 afterresult">
	<a href="<%=urll%>?STUDENTID=<%=id%>" class="white" style="color:white"><button type="button" class="btn btn-primary btn-lg "><%=lab%></button></a>
</div>
<%
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>