<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.* " %>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%
   
    String  firstname = request.getParameter("FIRSTNAME");
	String  surname = request.getParameter("SURNAME");
	String  stdid = request.getParameter("STUDENTID");
	String  email = request.getParameter("EMAIL");
	String  homephone = request.getParameter("HOMEPHONE");
	String  workphone = request.getParameter("WORKPHONE");
	String  mobilephone = request.getParameter("MOBILEPHONE");
	String  occupation = request.getParameter("OCCUPATION");
	
	String tt="";
	String  id = "";
	String myQuery="";
	String urll="";
	String lab="";

	if(request.getParameter("PARENTID")!=null)
   	{
   		id = request.getParameter("PARENTID");
   	}
	
	if(id =="")
	{
		tt="New Parents Record Inserted Successfully";
		myQuery = "INSERT INTO \"PARENTS\"( \"FIRSTNAME\", \"SURNAME\",\"EMAIL\", \"HOMEPHONE\", \"WORKPHONE\", \"MOBILEPHONE\",\"OCCUPATION\", \"STUDENTID\") VALUES (  ?,?, ?, ?, ?, ?, ?, ?);";
		urll="addemergency.jsp";
		lab="Add emergency";
	}
	else
	{
		tt="Parents Record Updated Successfully";
		myQuery = "UPDATE  \"PARENTS\" set  \"FIRSTNAME\"=?, \"SURNAME\"=?, \"EMAIL\"=?,\"HOMEPHONE\"=?, \"WORKPHONE\"=?, \"MOBILEPHONE\"=?,\"OCCUPATION\"=? WHERE \"PARENTID\"=?";
    	urll="parent.jsp";
    	lab="View parents";
	}
%>
<%
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";
	String myDataField = "Dejan";
	String generatedColumns[] = {"PARENTID"};
	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	myPreparedStatement = myConnection.prepareStatement(myQuery,generatedColumns);
	
	myPreparedStatement.setString(1, firstname);
	myPreparedStatement.setString(2, surname);
	myPreparedStatement.setString(3, email);
	myPreparedStatement.setString(4, homephone);
	myPreparedStatement.setString(5, workphone);
	myPreparedStatement.setString(6, mobilephone);
	myPreparedStatement.setString(7, occupation);
	int aInt = Integer.parseInt(stdid);
	myPreparedStatement.setInt(8, aInt);

	if(id !="")
	{
		int aIntt = Integer.parseInt(id);
		myPreparedStatement.setInt(8, aIntt);
	}
	myPreparedStatement.executeUpdate();
   	if(id=="")
	{
    	ResultSet rs = myPreparedStatement.getGeneratedKeys();
    	if(rs.next() && id=="")
		{
			long productId = rs.getLong(1);
     		id=stdid;
		}
	}
	myPreparedStatement.close();
	myConnection.close();
%>
<h2 id="beforeresult">Your Application is Processing...</h2>

<h3 style="display:none" class="afterresult"><%=tt%></h3>

<div class="col-sm-offset-4 col-sm-10 afterresult">
	<a href="<%=urll%>?STUDENTID=<%=id%>" class="white" style="color:white"><button type="button" class="btn btn-primary btn-lg "><%=lab%></button></a>
</div>
<%
}
catch(ClassNotFoundException e)
	{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>