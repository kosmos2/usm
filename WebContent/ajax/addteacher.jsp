<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.* " %>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%
    String  teacherid = request.getParameter("TEACHERID");
    String  firstname = request.getParameter("FIRSTNAME");
	String  surname = request.getParameter("SURNAME");
	
	String tt="";
	String  id = "";
	String myQuery="";
	String urll="";
	String lab="";
	if(request.getParameter("TEACHERID")!=null)
   	{
   		id = request.getParameter("TEACHERID");
   	}
	if(id =="")
	{
		tt="New Teachers Record Inserted Successfully";
		myQuery = "INSERT INTO \"TEACHERS\"( \"FIRSTNAME\", \"SURNAME\") VALUES ( ?,?);";
		urll="teacher.jsp";
		lab="View Teachers";
	}
	else
	{
		tt="Teachers Record Updated Successfully";
		myQuery = "UPDATE  \"TEACHERS\" set  \"FIRSTNAME\"=?, \"SURNAME\"=? WHERE \"TEACHERID\"=?";
		urll="teacher.jsp";
		lab="View Teachers";
	}
%>

<%
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";
	String myDataField = "Dejan";
	String generatedColumns[] = {"TEACHERID"};
	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	myPreparedStatement = myConnection.prepareStatement(myQuery,generatedColumns);
	
	
	myPreparedStatement.setString(1, firstname);
	myPreparedStatement.setString(2, surname);
	
	if(id !="")
	{
		int aIntt = Integer.parseInt(id);
		myPreparedStatement.setInt(3, aIntt);
	}
	myPreparedStatement.executeUpdate();
    if(id=="")
	{
    	ResultSet rs = myPreparedStatement.getGeneratedKeys();
    	if(rs.next() && id=="")
		{
	 		long productId = rs.getLong(1);
     		id=Long.toString(productId);
		}
	}
	myPreparedStatement.close();
	myConnection.close();
%>
<h2 id="beforeresult">Your Application is Processing...</h2>

<h3 style="display:none" class="afterresult"><%=tt%></h3>

<div class="col-sm-offset-4 col-sm-10 afterresult">
	<a href="<%=urll%>?TEACHERID=<%=id%>" class="white" style="color:white"><button type="button" class="btn btn-primary btn-lg "><%=lab%></button></a>
</div>
<%
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>