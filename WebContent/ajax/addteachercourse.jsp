<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.* " %>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%
    String  vid = request.getParameter("COURSE");
	String  tid = request.getParameter("TEACHERID");

	String tt="";
	String id="";
	String myQuery="";
	String urll="";
	String lab="";

	if(request.getParameter("TEACHERSKILLID")!=null)
   	{
   		id = request.getParameter("TEACHERSKILLID");
   	}
	if(id =="")
	{
		tt="New Record Inserted Successfully";
		myQuery = "INSERT INTO \"TEACHERSKILLS\"( \"TEACHERID\", \"VALUELISTID\") VALUES ( ?,?);";
		urll="teacher.jsp";
		lab="View Teacher";
	}
	else
	{
		tt="Teachers Record Updated Successfully";
		myQuery = "UPDATE  \"TEACHERSKILLS\" set  \"VALUELISTID\"=?, WHERE \"TEACHERSKILLID\"=?";
		urll="teacher.jsp";
		lab="View Teacher";
	}
%>

<%
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";

	String generatedColumns[] = {"TEACHERSKILLID"};
	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	myPreparedStatement = myConnection.prepareStatement(myQuery,generatedColumns);
		
	myPreparedStatement.setInt(1, Integer.parseInt(tid));
	myPreparedStatement.setInt(2, Integer.parseInt(vid));
	
	if(id !="")
	{
		int aIntt = Integer.parseInt(id);
		myPreparedStatement.setInt(2, aIntt);
	}
	myPreparedStatement.executeUpdate();
    if(id=="")
	{
    	ResultSet rs = myPreparedStatement.getGeneratedKeys();
    	if(rs.next() && id=="")
		{
	 		long productId = rs.getLong(1);
     		id=Long.toString(productId);
		}
	}
	myPreparedStatement.close();
	myConnection.close();
%>
<h2 id="beforeresult">Your Application is Processing...</h2>

<h3 style="display:none" class="afterresult"><%=tt%></h3>

<div class="col-sm-offset-4 col-sm-10 afterresult">
	<a href="<%=urll%>?TEACHERID=<%=tid%>" class="white" style="color:white"><button type="button" class="btn btn-primary btn-lg "><%=lab%></button></a>
</div>
<%
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>