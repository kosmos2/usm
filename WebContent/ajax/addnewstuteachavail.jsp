<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.* " %>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%
	String  sid = request.getParameter("STUDENTID");
	String  cid = request.getParameter("COURSEID");

	String tt="";
	String myInsertQuery="";
//	String myDeleteQuery="";
	String urll="";
	String lab="";
//	boolean isDeleted = false;

	tt="New Record Inserted Successfully";
//	myDeleteQuery = "DELETE FROM \"TEACHERAVAILABILITY\" where  \"TEACHERID\"='"+tid+"';";
	myInsertQuery = "INSERT INTO \"TEACHERSTUDENTS\"( \"TEACHERID\", \"STUDENTID\", \"DAY\", \"TIME\", \"COURSEID\") VALUES (?, ?, ?, ?, ?);";
	urll="student.jsp";
	lab="View Student";

	try {
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/USM";
		String username = "postgres";
		String password = "postgres";
	
		String generatedColumns[] = {"TEACHERSTUDENTID"};
		Connection myConnection = null;
		PreparedStatement myPreparedStatement = null;
		ResultSet myResultSet = null;
		Class.forName(driver).newInstance();
		myConnection = DriverManager.getConnection(url,username,password);
		
		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			if ((paramName.indexOf('-')) > -1) 
			{
				String[] paramValues = request.getParameterValues(paramName);	
				String paramValue = paramValues[0];
				if (!(paramValue.equals("")))
				{
					String[] parts = paramName.split("-");
					String part1 = parts[0]; // teacher
					String part2 = parts[1]; // day
					String part3 = parts[2]; // time

					myPreparedStatement = myConnection.prepareStatement(myInsertQuery,generatedColumns);
				
					myPreparedStatement.setInt(1, Integer.parseInt(part1));
					myPreparedStatement.setInt(2, Integer.parseInt(sid));
					myPreparedStatement.setString(3, part2);
					myPreparedStatement.setString(4, part3);
					myPreparedStatement.setInt(5, Integer.parseInt(cid));
									
				}
			}
		}
		myPreparedStatement.executeUpdate();
		myPreparedStatement.close();
		myConnection.close();
%>
<h2 id="beforeresult">Your Application is Processing...</h2>

<h3 style="display:none" class="afterresult"><%=tt%></h3>

<div class="col-sm-offset-4 col-sm-10 afterresult">
	<a href="<%=urll%>?STUDENTID=<%=sid%>" class="white" style="color:white"><button type="button" class="btn btn-primary btn-lg "><%=lab%></button></a>
</div>
<%
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>