<%! 
String metatitle ="Parent Profile";
%>
<%@ include file="includes/header.jsp" %>
<%@ include file="includes/top-row1.jsp" %>
<div class="container">
	<div class="col-sm-2"> 
 		<%@ include file="includes/right-tab1.jsp" %>
 	</div>
	<div class="col-sm-6"> 
		<%@ include file="includes/parent-information.jsp" %>
	</div>
</div>
<%@ include file="includes/footer.jsp" %>