<%! 
	String metatitle ="View Reports";
%>
<%@ include file="includes/header.jsp" %>
<div class="container">
	<div class="col-sm-12 top1"></div>
	<div class="col-sm-12">
		<h3>Here we will display a list of options for reports</h3>
		<%@ include file="includes/reporttable.jsp" %>
	</div>
</div>
<%@ include file="includes/footer.jsp" %>  