<%! 
	String metatitle ="View Emergencies";
%>
<%@ include file="includes/header.jsp" %>
<div class="container">
	<div class="col-sm-12 top1">
		<div class="col-sm-3">
			<h1>Display Emergencies</h1>
		</div>
		<div class="col-sm-9">
			<%@ include file="includes/table-emergency.jsp" %>
		</div>  
	</div>
</div>  
<%@ include file="includes/footer.jsp" %>