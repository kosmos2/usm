<div class="col-sm-12">
	<h1 class="blue">Add Student </h1>
 	<div class="">
   		<p><a class='btn btn-lg btn-primary addnewrow pull-right btn-new' href="displaystudents.jsp">View Students </a><br/><br/></p>
   	</div>
	<div class="record default_div">
		<form class="form-horizontal" id="addstudent" role="form" action="ajax/addstudent.jsp" method="get">
 <%
 String id ="";
  
if(request.getParameter("STUDENTID")!=null)
{
	id = request.getParameter("STUDENTID");
}

if(id!="")
{
	try
	{
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/USM";
		String username = "postgres";
		String password = "postgres";
		String myDataField = "Dejan";
		String myQuery = "SELECT * FROM \"STUDENTS\" WHERE \"STUDENTID\"='"+id+"'";
	
		Connection myConnection = null;
		PreparedStatement myPreparedStatement = null;
		ResultSet myResultSet = null;
		Class.forName(driver).newInstance();
		myConnection = DriverManager.getConnection(url,username,password);
		myPreparedStatement = myConnection.prepareStatement(myQuery);
		myResultSet = myPreparedStatement.executeQuery();
		myResultSet.next();
%>

		<input type="hidden" name="STUDENTID" id="STUDENTID" value="<%=myResultSet.getInt("STUDENTID")%>" />
		<div class="form-group">
			<label for="firstname" class="col-sm-2 control-label">First Name</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" id="FIRSTNAME"
		 		placeholder="Enter First Name" maxlength="50" name="FIRSTNAME" value="<%=myResultSet.getString("FIRSTNAME")%>">
		 		<p class="label label-danger FIRSTNAME">Enter First Name</p>
		 	</div>
		</div>
		<div class="form-group">
			<label for="middlename" class="col-sm-2 control-label">Middle Name</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" id="MIDDLENAME"
		 		placeholder="Enter Middle Name" maxlength="50" name="MIDDLENAME" value="<%=myResultSet.getString("MIDDLENAME")%>">
		 		<p class="label label-danger MIDDLENAME">Enter Middle Name</p>
		 	</div>
		</div>
		<div class="form-group">
			<label for="lastname" class="col-sm-2 control-label">Surname</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control"  maxlength="50" value="<%=myResultSet.getString("SURNAME")%>" name="SURNAME" id="SURNAME"
		 		placeholder="Enter Surname">
		 		<p class="label label-danger SURNAME">Enter Surname </p>
		 	</div>
		</div>
		<div class="form-group">
			<label for="street" class="col-sm-2 control-label">Street</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" value="<%=myResultSet.getString("STREET")%>"  maxlength="240" name="STREET" id="STREET"
		 			placeholder="Enter Street">
				<p class="label label-danger STREET">Enter street</p>
			</div>
		</div>
		<div class="form-group">
			<label for="suburb" class="col-sm-2 control-label">Suburb</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" value="<%=myResultSet.getString("SUBURB")%>"  maxlength="240" name="SUBURB" id="SUBURB"
		 			placeholder="Enter Suburb">
				<p class="label label-danger SUBURB">Enter suburb</p>
			</div>
		</div>
		<div class="form-group">
			<label for="postcode" class="col-sm-2 control-label">Postal Code</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" value="<%=myResultSet.getString("POSTCODE")%>" name="POSTCODE" id="POSTCODE"
					placeholder="Enter State" maxlength="4">
				<p class="label label-danger POSTCODE">Enter post code</p>
			</div>
		</div>
		<div class="form-group">
			<label for="state" class="col-sm-2 control-label">State</label>
			<div class="col-sm-10">
				<select name="STATE" id="STATE" class="form-control">
		    		<option value="<%=myResultSet.getString("STATE")%>"><%=myResultSet.getString("STATE")%></option>
				    <option value=""></option>
				    <option value="VIC">VIC</option>
				    <option value="NSW">NSW</option>
				    <option value="QLD">QLD</option>
				    <option value="SA">SA</option>
				    <option value="TAS">TAS</option>
				    <option value="WA">WA</option>
				    <option value="ACT">ACT</option>
				    <option value="NT">NT</option>
		  		</select>
		 		<p class="label label-danger STATE">Enter state</p>
		 	</div>
		 </div>
		 <div class="form-group">
		 	<label for="email" class="col-sm-2  control-label">Email</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" value="<%=myResultSet.getString("EMAIL")%>" maxlength="50" name="EMAIL" id="EMAIL"
		 			placeholder="Enter Email">
		 		<p class="label label-danger EMAIL">Enter valid email</p>
			</div>
		</div>
		<div class="form-group">
		 	<label for="dob" class="col-sm-2 control-label">Date of Birth</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" value="<%=myResultSet.getString("DOB")%>" max="10" name="DOB" id="DOB"
		 			placeholder="Enter DOB">
		 		<p class="label label-danger DOB">Select valid date of birth (in format DD-MM-YYYY)</p>
		 	</div>
		</div>
		<div class="form-group">
			<label for="homeph" class="col-sm-2 control-label">Home Phone</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" value="<%=myResultSet.getString("HOMEPHONE")%>" maxlength="10" name="HOMEPHONE" id="HOMEPHONE"
		 			placeholder="Enter Home phone">
		  		<p class="label label-danger HOMEPHONE">Enter home phone</p>
			</div>
		</div>
		<div class="form-group">
		 	<label for="workph" class="col-sm-2 control-label">Work Phone</label>
			<div class="col-sm-10">
		 		<input type="text" class="form-control" value="<%=myResultSet.getString("WORKPHONE")%>" maxlength="10"  name="WORKPHONE" id="WORKPHONE"
		 			placeholder="Enter Work phone">
		  		<p class="label label-danger WORKPHONE">Enter work phone</p>
			</div>
		</div>
		<div class="form-group">
		 	<label for="mobileph" class="col-sm-2 control-label">Mobile Phone</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" value="<%=myResultSet.getString("MOBILEPHONE")%>" maxlength="10"  name="MOBILEPHONE" id="MOBILEPHONE"
		 			placeholder="Enter Mobile phone">
		  		<p class="label label-danger MOBILEPHONE">Enter Mobile phone</p>
			</div>
		</div>
		<div class="form-group">
			<label for="isgroupok" class="col-sm-2 control-label">Participate in group lessons?</label>
 			<div class="col-sm-10">
 				<input type="checkbox" class="form-control" id="ISGROUPOK" maxlength="1" name="ISGROUPOK" value="true" 
<%		if (myResultSet.getBoolean("ISGROUPOK")) 
 		{
%> 		checked >
<% 
 		}
 		else
 		{
%>
 		>
<%
 		}
%>
 			</div>
		</div>
<%	
		myResultSet.close();
		myPreparedStatement.close();
		myConnection.close();
	}
	catch(ClassNotFoundException e)
	{
		e.printStackTrace();
	}

}
else
{
%>
		<input type="hidden" name="STUDENTID" id="STUDENTID" value="" />
		<div class="form-group">
			<label for="firstname" class="col-sm-2 control-label">First Name</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="FIRSTNAME" placeholder="Enter First Name" maxlength="50" name="FIRSTNAME" value="">
				<p class="label label-danger FIRSTNAME">Enter First Name</p>
			</div>
		</div>
		<div class="form-group">
			<label for="middlename" class="col-sm-2 control-label">Middle Name</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="MIDDLENAME" placeholder="Enter Middle Name" maxlength="50" name="MIDDLENAME" value="">
				<p class="label label-danger MIDDLENAME">Enter Middle Name</p>
			</div>
		</div>
		<div class="form-group">
		 	<label for="lastname" class="col-sm-2 control-label">Surname</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control"  maxlength="50" value="" name="SURNAME" id="SURNAME" placeholder="Enter Surname">
		 		<p class="label label-danger SURNAME">Enter Surname </p>
			</div>
		</div>
		<div class="form-group">
			<label for="street" class="col-sm-2 control-label">Street</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" value=""  maxlength="240" name="STREET" id="STREET" placeholder="Enter Street">
		 		<p class="label label-danger STREET">Enter Street</p>
		 	</div>
		</div>
		<div class="form-group">
			 <label for="suburb" class="col-sm-2 control-label">Suburb</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" value=""  maxlength="240" name="SUBURB" id="SUBURB" placeholder="Enter Suburb">
		 		<p class="label label-danger SUBURB">Enter Suburb</p>
		 	</div>
		</div>
		<div class="form-group">
		 	<label for="postalcode" class="col-sm-2 control-label">Postal Code</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" value="" name="POSTCODE" id="POSTCODE" placeholder="Enter Postal Code" maxlength="4">
		 		<p class="label label-danger POSTCODE">Enter POSTCODE</p>
		 	</div>
		</div>
		<div class="form-group">
			<label for="state" class="col-sm-2 control-label">State</label>
		 	<div class="col-sm-10">
		   		<select name="STATE" id="STATE" class="form-control">
			    	<option selected="selected">Enter State</option>
				    <option value="VIC">VIC</option>
				    <option value="NSW">NSW</option>
				    <option value="QLD">QLD</option>
				    <option value="SA">SA</option>
				    <option value="TAS">TAS</option>
				    <option value="WA">WA</option>
				    <option value="ACT">ACT</option>
				    <option value="NT">NT</option>
		  		</select>
		 		<p class="label label-danger STATE">Enter State</p>
			</div>
		</div>
		<div class="form-group">
		 	<label for="email" class="col-sm-2  control-label">Email</label>
		 	<div class="col-sm-10">
				<input type="text" class="form-control" value="" maxlength="50" name="EMAIL" id="EMAIL" placeholder="Enter Email">
		 		<p class="label label-danger EMAIL">Enter valid email</p>
			</div>
		</div>
		<div class="form-group">
		 	<label for="dob" class="col-sm-2 control-label">Date of Birth</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" value="" max="10" name="DOB" id="DOB" placeholder="Enter Date of Birth">
		 		<p class="label label-danger DOB">Select valid date of birth (in format DD-MM-YYYY)</p>
		 	</div>
		</div> 
		<div class="form-group">
		 	<label for="homeph" class="col-sm-2 control-label">Home Phone</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" value="" maxlength="10" name="HOMEPHONE" id="HOMEPHONE" placeholder="Enter Homephone">
		  		<p class="label label-danger HOMEPHONE">Enter home phone</p>
			</div>
		</div>
		<div class="form-group">
		 	<label for="workph" class="col-sm-2 control-label">Work Phone</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" value="" maxlength="10"  name="WORKPHONE" id="WORKPHONE" placeholder="Enter Work phone">
		  		<p class="label label-danger WORKPHONE">Enter work phone</p>
		 	</div>
		</div>
		<div class="form-group">
		 	<label for="mobileph" class="col-sm-2 control-label">Mobile Phone</label>
		 	<div class="col-sm-10">
		 		<input type="text" class="form-control" value="" maxlength="10"  name="MOBILEPHONE" id="MOBILEPHONE" placeholder="Enter Mobile phone">
		  		<p class="label label-danger MOBILEPHONE">Enter mobile phone</p>
			</div>
		</div>
		<div class="form-group">
			<label for="isgroupok" class="col-sm-2 control-label">Participate in group lessons?</label>
		 	<div class="col-sm-10">
		 		<input type="checkbox" class="form-control" id="ISGROUPOK" maxlength="1" name="ISGROUPOK" value="true">
		 	</div>
		</div>
<%
}
%>
 		<div class="form-group">
 			<div class="col-sm-offset-2 col-sm-10">
 				<button type="submit" name="addstudent" class="btn btn-primary btn-lg ">
 					Save
 				</button><br/>
 			</div>
		</div>
	</form>
	</div>
	<div class="row ajax" style="display:none">
		<div class="col-sm-offset-2 col-md-8">
  			<div id="status"></div>
 		</div>
	</div>
</div>