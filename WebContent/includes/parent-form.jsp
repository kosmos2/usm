 <div class="col-sm-12">
 	<h1 class="blue">Add Parent </h1>
 	<div class="">
   		<p><a class='btn btn-lg btn-primary addnewrow pull-right btn-new' href="student.jsp?STUDENTID=<%=request.getParameter("STUDENTID") %>">Back</a><br/><br/></p>
   	</div>
  	<div class="record default_div">
		<form class="form-horizontal" id="addparent" role="form" action="ajax/addparent.jsp" method="get">
 <%
 String id ="";
  
if(request.getParameter("PARENTID")!=null)
{
  
id = request.getParameter("PARENTID");
  
}


if(id!="")
{
	try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";

	String myQuery = "SELECT * FROM \"PARENTS\" WHERE \"PARENTID\"='"+id+"'";
	
	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	myPreparedStatement = myConnection.prepareStatement(myQuery);
	myResultSet = myPreparedStatement.executeQuery();
	myResultSet.next();
    
%>
			<input type="hidden" name="PARENTID" id="PARENTID" value="<%=myResultSet.getInt("PARENTID")%>" />
			<input type="hidden" name="STUDENTID" id="STUDENTID" value="<%=myResultSet.getInt("STUDENTID")%>" />

 			<div class="form-group">
 				<label for="firstname" class="col-sm-2 control-label">First Name</label>
 				<div class="col-sm-10">
 					<input type="text" class="form-control" id="FIRSTNAME"
 						placeholder="Enter First Name" maxlength="50" name="FIRSTNAME" value="<%=myResultSet.getString("FIRSTNAME")%>">
 					<p class="label label-danger FIRSTNAME">Enter First Name</p>
 				</div>
	 		</div>
 			<div class="form-group">
 				<label for="lastname" class="col-sm-2 control-label">Surname</label>
 				<div class="col-sm-10">
 					<input type="text" class="form-control"  maxlength="50" value="<%=myResultSet.getString("SURNAME")%>" name="SURNAME" id="SURNAME"
 						placeholder="Enter Surname">
 					<p class="label label-danger SURNAME">Enter Surname </p>
 				</div>
 			</div>
 			<div class="form-group">
 				<label for="lastname" class="col-sm-2  control-label">Email</label>
 				<div class="col-sm-10">
 					<input type="text" class="form-control" value="<%=myResultSet.getString("EMAIL")%>" maxlength="50" name="EMAIL" id="EMAIL"
 						placeholder="Enter Email">
 					<p class="label label-danger EMAIL">Enter Valid Email</p>
 				</div>
 			</div>
 			<div class="form-group">
 				<label for="lastname" class="col-sm-2 control-label">Home Phone</label>
 				<div class="col-sm-10">
 					<input type="text" class="form-control" value="<%=myResultSet.getString("HOMEPHONE")%>" maxlength="10" name="HOMEPHONE" id="HOMEPHONE"
 						placeholder="Enter Homephone">
  					<p class="label label-danger HOMEPHONE">Enter Home phone</p>
 				</div>
 			</div>
 			<div class="form-group">
 <label for="lastname" class="col-sm-2 control-label">Work Phone</label>
 <div class="col-sm-10">
 <input type="text" class="form-control" value="<%=myResultSet.getString("WORKPHONE")%>" maxlength="10"  name="WORKPHONE" id="WORKPHONE"
 placeholder="Enter Work phone">
  <p class="label label-danger WORKPHONE">Enter work phone</p>
 </div>
 </div>
 <div class="form-group">
 <label for="lastname" class="col-sm-2 control-label">Mobile Phone</label>
 <div class="col-sm-10">
 <input type="text" class="form-control" value="<%=myResultSet.getString("MOBILEPHONE")%>" maxlength="10"  name="MOBILEPHONE" id="MOBILEPHONE"
 placeholder="Enter Mobile phone">
  <p class="label label-danger MOBILEPHONE">Enter Mobile phone</p>
 </div>
 </div>
 <div class="form-group">
 <label for="Occupation" class="col-sm-2 control-label">Occupation</label>
 <div class="col-sm-10">
 <input type="text" class="form-control"  maxlength="50" value="<%=myResultSet.getString("OCCUPATION")%>" name="OCCUPATION" id="OCCUPATION"
 placeholder="Enter Occupation">
 <p class="label label-danger OCCUPATION">Enter Occupation </p>
 </div>
 </div>

<%	
  
myResultSet.close();
myPreparedStatement.close();
myConnection.close();
}
catch(ClassNotFoundException e)
	{
	e.printStackTrace();
}

}
else
{
	%>
	<input type="hidden" name="PARENTID" id="PARENTID" value="" />
    <input type="hidden" name="STUDENTID" id="STUDENTID" value="<%=request.getParameter("STUDENTID")%>" />
 
 <div class="form-group">
 <label for="firstname" class="col-sm-2 control-label">First Name</label>
 <div class="col-sm-10">
 <input type="text" class="form-control" id="FIRSTNAME"
 placeholder="Enter First Name" maxlength="50" name="FIRSTNAME" value="">
 <p class="label label-danger FIRSTNAME">Enter First Name</p>
 </div>
 </div>
 <div class="form-group">
 <label for="lastname" class="col-sm-2 control-label">Surname</label>
 <div class="col-sm-10">
 <input type="text" class="form-control"  maxlength="50" value="" name="SURNAME" id="SURNAME"
 placeholder="Enter Surname">
 <p class="label label-danger SURNAME">Enter Surname </p>
 </div>
 </div>

 <div class="form-group">
 <label for="lastname" class="col-sm-2  control-label">Email</label>
 <div class="col-sm-10">
 <input type="text" class="form-control" value="" maxlength="50" name="EMAIL" id="EMAIL"
 placeholder="Enter Email">
 <p class="label label-danger EMAIL">Enter Valid Email</p>
 </div>
 </div>
 <div class="form-group">
 <label for="lastname" class="col-sm-2 control-label">Home Phone</label>
 <div class="col-sm-10">
 <input type="text" class="form-control" value="" maxlength="10" name="HOMEPHONE" id="HOMEPHONE"
 placeholder="Enter Homephone">
  <p class="label label-danger HOMEPHONE">Enter Home phone</p>
 </div>
 </div>
 <div class="form-group">
 <label for="lastname" class="col-sm-2 control-label">Work Phone</label>
 <div class="col-sm-10">
 <input type="text" class="form-control" value="" maxlength="10"  name="WORKPHONE" id="WORKPHONE"
 placeholder="Enter Work phone">
  <p class="label label-danger WORKPHONE">Enter work phone</p>
 </div>
 </div>
 <div class="form-group">
 <label for="lastname" class="col-sm-2 control-label">Mobile Phone</label>
 <div class="col-sm-10">
 <input type="text" class="form-control" value="" maxlength="10"  name="MOBILEPHONE" id="MOBILEPHONE"
 placeholder="Enter Mobile phone">
  <p class="label label-danger MOBILEPHONE">Enter Mobile phone</p>
 </div>
 </div>
 
  <div class="form-group">
 <label for="Occupation" class="col-sm-2 control-label">Occupation</label>
 <div class="col-sm-10">
 <input type="text" class="form-control"  maxlength="50" value="" name="OCCUPATION" id="OCCUPATION"
 placeholder="Enter Occupation">
 <p class="label label-danger OCCUPATION">Enter Occupation </p>
 </div>
 </div>
    <%
}
%>

 <div class="form-group">
 <div class="col-sm-offset-2 col-sm-10">
 <button type="submit" name="addParent" class="btn btn-primary btn-lg ">
 Save
 </button><br/>
 </div>
 </div>
  
</form>
</div>
<div class="row ajax" style="display:none">
<div class="col-sm-offset-2 col-md-8">
   
  <div id="status"></div>
 </div>
</div>
</div>