<%
String id ="";
  
if(request.getParameter("STUDENTID")!=null)
{
	id = request.getParameter("STUDENTID");
}
String st_name="";
String st_mname="";
String st_sname="";
String st_street="";
String st_suburb="";
String st_post="";
String st_state="";
String st_email="";
String st_dob="";
String st_home="";
String st_work="";
String st_mobile="";
boolean st_groupOK=false;

if(id!="")
{
	try {
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/USM";
		String username = "postgres";
		String password = "postgres";
	
		String myQuery = "SELECT * FROM \"STUDENTS\" WHERE \"STUDENTID\"='"+id+"'";
		String coursesQuery = "SELECT v.\"VALUE\" FROM \"STUDENTCOURSES\" sc, \"VALUELIST\" v WHERE sc.\"STUDENTID\"='"+
			id+"' and v.\"VALUELISTID\"=sc.\"COURSEID\"";
		
		Connection myConnection = null;
		PreparedStatement myPreparedStatement = null;
		ResultSet myResultSet = null;
		Class.forName(driver).newInstance();
		myConnection = DriverManager.getConnection(url,username,password);
		myPreparedStatement = myConnection.prepareStatement(myQuery);
		myResultSet = myPreparedStatement.executeQuery();
		
		while(myResultSet.next()) {
		    st_name=myResultSet.getString("FIRSTNAME");
		    st_mname=myResultSet.getString("MIDDLENAME");
		    st_sname=myResultSet.getString("SURNAME");
			st_street=myResultSet.getString("STREET");
			st_suburb=myResultSet.getString("SUBURB");
			st_post=myResultSet.getString("POSTCODE");
			st_state=myResultSet.getString("STATE");
			st_email=myResultSet.getString("EMAIL");
			//st_dob=myResultSet.getDate("DOB");
			st_dob=myResultSet.getString("DOB");
			st_work=myResultSet.getString("WORKPHONE");
			st_home=myResultSet.getString("HOMEPHONE");
			st_mobile=myResultSet.getString("MOBILEPHONE");
			st_groupOK=myResultSet.getBoolean("ISGROUPOK");
	   }
%>

<div class="student">

<!-- <div class="col-sm-2 col-xs-3"><img src="images/user.png" class="img-responsive" style="width:150px"/></div> -->
	<div class="col-sm-10 col-xs-9">
		<div class="panel panel-default">
 			<div class="panel-heading white bg1">Student Information</div>
 			<table class="table">
 
				<tr><td><span class="blue"><b>Name: </b></span><%=st_name%></td></tr>
				<tr><td><span class="blue"><b>Middle Name: </b></span><%=st_mname%></td></tr>
				<tr><td><span class="blue"><b>Surname : </b></span><%=st_sname%></td></tr>
				<tr><td><span class="blue"><b>Street : </b></span><%=st_street%></td></tr>
				<tr><td><span class="blue"><b>Suburb: </b></span><%=st_suburb%></td></tr>
				<tr><td><span class="blue"><b>Post Code: </b></span><%=st_post%></td></tr>
				<tr><td><span class="blue"><b>State: </b></span><%=st_state%></td></tr>
				<tr><td><span class="blue"><b>Email: </b></span><%=st_email%></td></tr>
				<tr><td><span class="blue"><b>DOB: </b></span><%=st_dob%></td></tr>
				<tr><td><span class="blue"><b>Home Phone: </b></span><%=st_home%></td></tr>
				<tr><td><span class="blue"><b>Work Phone: </b></span><%=st_work%></td></tr>
				<tr><td><span class="blue"><b>Mobile Phone: </b></span><%=st_mobile%></td></tr>
				<tr><td><span class="blue"><b>Group lessons? </b></span><%=st_groupOK%></td></tr>
				<tr><td><span class="blue"><a href="displayparents.jsp?STUDENTID=<%=id%>">[Parental Details]</a></span></td></tr>
				<tr><td><a href="displayemergency.jsp?STUDENTID=<%=id%>">[Emergency Details]</a> </td></tr>
	 
			</table>
		</div>
<%		
		myResultSet.close();
		myPreparedStatement.close();
		myConnection.close();
	}
	catch(ClassNotFoundException e)
	{
		e.printStackTrace();
	}
	catch (SQLException ex)
	{
		out.print("SQLException: "+ex.getMessage());
		out.print("SQLState: " + ex.getSQLState());
		out.print("VendorError: " + ex.getErrorCode());
	} 
}
%>

	</div>
</div>