<div class="top1">
	<p><button class='btn btn-lg btn-primary addnewcourse pull-right btn-new'>Add New <span class="glyphicon glyphicon-plus"></span></button><br/><br/></p>
</div>


 
<div class="table-responsive top">
 	<table class="table table-striped" id="myTable">
 		<thead>
		<tr>
			
 			<th>Course Name</th>
 			<th>Description</th>
  			<th>Edit</th>
   			<th>Delete</th>
		</tr>
 		</thead>
 		<tbody>
  <%
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";
	String myQuery = "SELECT * FROM \"VALUELIST\" WHERE \"NAME\"='COURSES'";

	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	myPreparedStatement = myConnection.prepareStatement(myQuery);
	myResultSet = myPreparedStatement.executeQuery();
    while(myResultSet.next()) {
%>

 		<tr class="tab-row tab-row-<%=myResultSet.getInt("VALUELISTID") %>">
<%--   			<td class=""><a href="courses.jsp?VALUELISTID=<%=myResultSet.getInt("VALUELISTID") %>"  data-id="<%=myResultSet.getInt("VALUELISTID") %>" class="btn btn-success">View More</a></td> --%>
   			<td ><%=myResultSet.getString("VALUE") %></td>
   			<td ><%=myResultSet.getString("DESCRIPTION") %></td>
			<td><a href="addnewcourse.jsp?VALUELISTID=<%=myResultSet.getInt("VALUELISTID") %>"><div class='glyphicon glyphicon-pencil'></div></a></td>
 			<td class='deleterow'><a data-url="course" data-id="<%=myResultSet.getInt("VALUELISTID") %>" href="javascript:void(0)"><div class='glyphicon glyphicon-remove'></div></a></td>
 		</tr>

<% 
	} 

	myResultSet.close();
	myPreparedStatement.close();
	myConnection.close();
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>
		</tbody>
	</table>
</div>