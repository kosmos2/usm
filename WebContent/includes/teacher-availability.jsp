<%
 String teacherId ="";
  
if(request.getParameter("TEACHERID")!=null)
{
	teacherId = request.getParameter("TEACHERID");
}
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";
	
	String daysQuery = "SELECT * FROM \"VALUELIST\" where \"NAME\"='DAYS'";
	String teacherQuery = "SELECT * FROM \"TEACHERS\" where \"TEACHERID\"='"+teacherId+"'";
	
	Connection myConnection = null;
	PreparedStatement prepStatmDays = null;
	ResultSet rsDays = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);

	PreparedStatement prepStatmTeach = null;
	ResultSet rsTeach = null;	
	prepStatmTeach = myConnection.prepareStatement(teacherQuery);
	rsTeach = prepStatmTeach.executeQuery();	
	if (rsTeach.next())
	{

%>
	<div class="col-sm-3">
		<h1><span class="blue"><%=rsTeach.getString("FIRSTNAME") %>,</span>&nbsp;<span class="blue"><%=rsTeach.getString("SURNAME") %></span></h1>
	</div>
<%
	}
%>
<div class="student">
	<div class="col-sm-12 col-xs-9">
		<div class="panel panel-default">
 			<div class="panel-heading white bg1"><span class="white">Teacher Availability</span></div>
			<div class="record default_div">
				<form class="form-horizontal" id="addnewteacheravail" role="form" action="ajax/addteacheravail.jsp" method="get">
					<input type="hidden" name="TEACHERID" id="TEACHERID" value="<%=teacherId%>" />
   			<div class="table">
 				<table class="table">
			 		<thead>
						<tr>
							<th></th>
							<th>MONDAY</th>
							<th>TUESDAY</th>
							<th>WEDNESDAY</th>
							<th>THURSDAY</th>
							<th>FRIDAY</th>
							<th>SATURDAY</th>
							<th>SUNDAY</th>		   				
						</tr>
			 		</thead>
			 		<tbody>
<%
	if (rsTeach != null) rsTeach.close();
	if (prepStatmTeach != null) prepStatmTeach.close();
	String timesQuery = "SELECT * FROM \"VALUELIST\" where \"NAME\"='TIMES'";
	
	PreparedStatement prepStatmTimes = null;
	ResultSet rsTimes = null;

	

%> 
						<tr class="tab-row tab-row-<%=teacherId %>">
							<th><div class="col-sm-12">TIME</div></th>
<%
		prepStatmDays = myConnection.prepareStatement(daysQuery);
		rsDays = prepStatmDays.executeQuery();
    	while(rsDays.next())
    	{
%>
							<th>
							<div class="form-group">
								<div class="col-sm-12">
									<select size="25" multiple name="<%=rsDays.getString("VALUE") %>" id="<%=rsDays.getString("VALUE") %>" class="form-control">
<%
			prepStatmTimes = myConnection.prepareStatement(timesQuery);
			rsTimes = prepStatmTimes.executeQuery();
    		while(rsTimes.next())
    	    {
//        	String teachAvailQuery = "SELECT * FROM \"TEACHERAVAILABILITY\" where \"TEACHERID\"='"+teacherId+"' and \"DAY\"='"+rsDays.getString("VALUE")+"' and \"TIME\"='"+rsTimes.getString("VALUE")+"'";
//        	PreparedStatement psTeachAvail = null;
//        	ResultSet rsTeachAvail = null;
//        	psTeachAvail = myConnection.prepareStatement(teachAvailQuery);
//        	rsTeachAvail = psTeachAvail.executeQuery();    		
%>

<%
//			if (rsTeachAvail.next())
//			{
%>	
								    	<option value="<%=rsTimes.getString("VALUE") %>"><%=rsTimes.getString("VALUE") %></option>
<!--								    	<option value=""></option>  -->
<%
//			}
//			else
//			{
%>
<!-- 										<option value=""></option>
								    	<option value="<%=rsTimes.getString("VALUE") %>">OK</option>  -->
<%
//			}
//			if (rsTeachAvail != null) rsTeachAvail.close();
//			if (psTeachAvail != null) psTeachAvail.close();
%>

<%
    	}
%>
								    </select>
								</div>
							</div>
							</th>
<%
		rsTimes.close();
   		prepStatmTimes.close();
	} 
   	rsDays.close();
   	prepStatmDays.close();

	myConnection.close();
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>
					</tbody>
				</table>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" name="addnewteacheravail" class="btn btn-primary btn-lg ">Save</button><br/>
			 	</div>
			</div>
			</form>
			</div>
			<div class="row ajax" style="display:none">
				<div class="col-sm-offset-2 col-md-8">
			  		<div id="status"></div>
			 	</div>
			</div>
		</div>
	</div>
</div>