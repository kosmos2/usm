 <div class="col-sm-12">
 	<h1 class="blue">Add Course</h1>
 	<div class="">
   		<p><a class='btn btn-lg btn-primary addnewrow pull-right btn-new' href="displayteachers.jsp">View Teachers</a><br/><br/></p>
   	</div>
	<div class="record default_div">
		<form class="form-horizontal" id="addnewteachercourse" role="form" action="ajax/addteachercourse.jsp" method="get">
 <%
 	String id ="";
	String teacherId ="";
	if(request.getParameter("VALUELISTID")!=null)
	{
		id = request.getParameter("VALUELISTID");
	}
	if(request.getParameter("TEACHERID")!=null)
	{
		teacherId = request.getParameter("TEACHERID");
	}
	if(id!="")
	{
		try 
		{
			String driver = "org.postgresql.Driver";
			String url = "jdbc:postgresql://localhost:5432/USM";
			String username = "postgres";
			String password = "postgres";
			String myQuery = "SELECT * FROM \"VALUELIST\" WHERE \"VALUELISTID\"='"+id+"'";
	
			Connection myConnection = null;
			PreparedStatement myPreparedStatement = null;
			ResultSet myResultSet = null;
			Class.forName(driver).newInstance();
			myConnection = DriverManager.getConnection(url,username,password);
			myPreparedStatement = myConnection.prepareStatement(myQuery);
			myResultSet = myPreparedStatement.executeQuery();
			myResultSet.next();
%>

<input type="hidden" name="VALUELISTID" id="VALUELISTID" value="<%=myResultSet.getInt("VALUELISTID")%>" />
 <div class="form-group">
 	<label for="coursename" class="col-sm-2 control-label">Course Name</label>
 	<div class="col-sm-10">
 		<input type="text" class="form-control" id="VALUE"
 			placeholder="Enter studentid" maxlength="50" name="courseid" value="<%=myResultSet.getString("VALUELISTID")%>">
 		<p class="label label-danger">Enter Course</p>
 	</div>
 </div>
 <div class="form-group">
 	<label for="coursedesc" class="col-sm-2 control-label">Description</label>
 	<div class="col-sm-10">
 		<input type="text" class="form-control"  maxlength="50" value="<%=myResultSet.getString("DESCRIPTION")%>" name="DESCRIPTION" id="DESCRIPTION"
 			placeholder="Enter Description">
 		<p class="label label-danger">Enter Description</p>
 	</div>
 </div>

<%	
  
			myResultSet.close();
			myPreparedStatement.close();
			myConnection.close();
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}

	}
	else
	{
%>
	
	<input type="hidden" name="VALUELISTID" id="VALUELISTID" value="" />
	<input type="hidden" name="TEACHERID" id="TEACHERID" value="<%=teacherId%>" />
	<div class="form-group">
		<label for="Course" class="col-sm-2 control-label">Course</label>
 		<div class="col-sm-10">
<%
		try 
		{
			String driver = "org.postgresql.Driver";
			String url = "jdbc:postgresql://localhost:5432/USM";
			String username = "postgres";
			String password = "postgres";
			String allCourses = "SELECT * FROM \"VALUELIST\" WHERE \"NAME\"='COURSES' and \"VALUELISTID\" " 
					+"not in (select \"VALUELISTID\" FROM \"TEACHERSKILLS\" WHERE \"TEACHERID\"='"+teacherId+"')";
	
			Connection myConnection = null;
			PreparedStatement myPreparedStatement = null;
			ResultSet myResultSet = null;
			Class.forName(driver).newInstance();
			myConnection = DriverManager.getConnection(url,username,password);
			myPreparedStatement = myConnection.prepareStatement(allCourses);
			myResultSet = myPreparedStatement.executeQuery();

%>
 		
	   		<select name="COURSE" id="COURSE" class="form-control">
	   			<option value=""></option>
<%	   			
			while(myResultSet.next()) 
			{
%>
	    		<option value="<%=myResultSet.getString("VALUELISTID")%>"><%=myResultSet.getString("VALUE")%></option>
<%
			}
			myResultSet.close();
			myPreparedStatement.close();
			myConnection.close();
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException ex)
		{
			out.print("SQLException: "+ex.getMessage());
			out.print("SQLState: " + ex.getSQLState());
			out.print("VendorError: " + ex.getErrorCode());
		}
%>
	  		</select>
			<p class="label label-danger COURSE">Enter Course</p>
 		</div>
	</div>
<%
}
%>

<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		<button type="submit" name="addnewteachercourse" class="btn btn-primary btn-lg ">Submit</button><br/>
 	</div>
</div>  
</form>
</div>
<div class="row ajax" style="display:none">
	<div class="col-sm-offset-2 col-md-8">
  		<div id="status"></div>
 	</div>
</div>
</div>