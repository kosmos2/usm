<%
String teacherID ="";
String studentID ="";
String courseId ="";
String dayID ="";
String dayName ="";
String dayDescription ="";
String studentName ="";
boolean groupOK = false;

if(request.getParameter("STUDENTID")!=null)
{
	studentID = request.getParameter("STUDENTID");
}
if(request.getParameter("COURSE")!=null)
{
	courseId = request.getParameter("COURSE");
}
//if(request.getParameter("DAY")!=null)
//{
//	dayID = request.getParameter("DAY");
//}
String[] paramValues = request.getParameterValues("DAY");
if (paramValues != null)
{
	for(int i=0; i<paramValues.length; i++)
	{
		if (i == 0)
			dayID = dayID + paramValues[i];
		else
			dayID = dayID + ", " + paramValues[i];
	}
}
else
	dayID = "4";
try 
{
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";
	
	String studentQuery = "SELECT * FROM \"STUDENTS\" where \"STUDENTID\"='"+studentID+"'";
	String daysQuery = "SELECT * FROM \"VALUELIST\" where \"NAME\"='DAYS' and \"VALUELISTID\" in ("+dayID+")";

	Connection myConnection = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);

	PreparedStatement prepStatmStudent = null;
	ResultSet rsStudent = null;
	prepStatmStudent = myConnection.prepareStatement(studentQuery);
	rsStudent = prepStatmStudent.executeQuery();	
	if (rsStudent.next())
	{
		studentName = rsStudent.getString("FIRSTNAME")+" "+rsStudent.getString("SURNAME");
		groupOK = rsStudent.getBoolean("ISGROUPOK");
	}
	rsStudent.close();
	prepStatmStudent.close();	

	PreparedStatement prepStatmTimes = null;
	ResultSet rsTimes = null;
	PreparedStatement prepStatmTeach = null;
	ResultSet rsTeach = null;

	PreparedStatement prepStatmDays = null;
	ResultSet rsDays = null;
	prepStatmDays = myConnection.prepareStatement(daysQuery);
	rsDays = prepStatmDays.executeQuery();
	
%>
<div class="record default_div">
	<form class="form-horizontal" id="addnewstuteachavail" role="form" action="ajax/addnewstuteachavail.jsp" method="get">
		<input type="hidden" name="STUDENTID" id="STUDENTID" value="<%=studentID%>" />
		<input type="hidden" name="COURSEID" id="COURSEID" value="<%=courseId%>" />
		<div class="col-sm-12">
		<table class="table">
			<tr>
<%
	
	while (rsDays.next())
	{
		dayName = rsDays.getString("VALUE");
		dayDescription = rsDays.getString("DESCRIPTION");

		String teacherQuery = "select * from \"TEACHERS\" where \"TEACHERID\"  in (select \"TEACHERID\" from \"TEACHERSKILLS\" where \"VALUELISTID\"="+courseId+") AND "+
				"\"TEACHERID\" in (select \"TEACHERID\" from \"TEACHERAVAILABILITY\" where \"DAY\" = '"+dayName+"')";
%>
				<td>
<%
		prepStatmTeach = myConnection.prepareStatement(teacherQuery);
		rsTeach = prepStatmTeach.executeQuery();
		while (rsTeach.next())
		{
			teacherID = rsTeach.getString("TEACHERID");
%>
		<div class="student">
			<div class="col-sm-12 col-xs-9">
				<div class="panel panel-default">
 					<div class="panel-heading white bg1"><span class="white">Teacher Availability</span></div> 
  					<div class="table">
						<table class="table">
							<thead>
								<tr>
									<th>
										<span class="blue"><%=rsTeach.getString("FIRSTNAME") %>,</span>&nbsp;<span class="blue"><%=rsTeach.getString("SURNAME") %></span>
									</th>
									<th><%=dayDescription %></th>
								</tr>
							</thead>
							<tbody>
<%
			String timesQuery = "select * from \"TEACHERAVAILABILITY\" where \"TEACHERID\" ='"+teacherID+"' and \"DAY\" = '"+dayName+"'";
			prepStatmTimes = myConnection.prepareStatement(timesQuery);
			rsTimes = prepStatmTimes.executeQuery();
			
		    while(rsTimes.next())
		    {
%> 
								<tr class="tab-row tab-row-<%=teacherID %>">
									<th><div class="col-sm-12"><%=rsTimes.getString("TIME") %></div></th>
<%
				String teachStudentQuery = "SELECT * FROM \"TEACHERSTUDENTS\" where \"TEACHERID\"='"+teacherID+"' and \"DAY\"='"+dayName+"' and \"TIME\"='"+rsTimes.getString("TIME")+"'";
		        PreparedStatement psTeachStudent = null;
		        ResultSet rsTeachStudent = null;
		        psTeachStudent = myConnection.prepareStatement(teachStudentQuery);
		        rsTeachStudent = psTeachStudent.executeQuery();
%>
									<th>
										<div class="form-group">
											<div class="col-sm-12">
												<select name="<%=teacherID %>-<%=dayName %>-<%=rsTimes.getString("TIME") %>" id="<%=dayName %>-<%=rsTimes.getString("TIME") %>" class="form-control">
<%
				if (!rsTeachStudent.next())
				{
%>
													<option value=""></option>
			   										<option value="<%=studentID %>"><%=studentName %></option>
<%				
				}
				else
				{
					if (!groupOK)
					{
%>	
					   								<option value="" disabled>NOT AVAILABLE</option>
<%
					}
					else
					{
						String students = "";
						do
						{
							students = students+rsTeachStudent.getString("STUDENTID")+",";
						} while (rsTeachStudent.next());
						//remove last comma
						students = students.substring(0, students.length()-1);
						
						String existingStudentQuery = "SELECT \"ISGROUPOK\", \"FIRSTNAME\", \"SURNAME\" FROM \"STUDENTS\" where \"STUDENTID\" in ("+students+")";;
						boolean groupLesson = false;
						String groupString = "";
						PreparedStatement psExistingStudent = null;
						ResultSet rsExistingStudent = null;
						psExistingStudent = myConnection.prepareStatement(existingStudentQuery);
						rsExistingStudent = psExistingStudent.executeQuery();
						while (rsExistingStudent.next())
						{
							if (rsExistingStudent.getBoolean("ISGROUPOK"))
							{
								groupLesson = true;
								groupString = groupString + rsExistingStudent.getString("FIRSTNAME")+" "+rsExistingStudent.getString("SURNAME")+",";
							}
						}
						if (groupLesson)
						{
%>
													<option value=""><%=groupString %></option>
													<option value="<%=studentID %>"><%=groupString %>&nbsp;<%=studentName %></option>
<%
						}
						else
						{
%>
													<option value="" disabled>NOT AVAILABLE</option>
<%					
						}					
						if (rsExistingStudent != null) rsExistingStudent.close();
						if (psExistingStudent != null) psExistingStudent.close();
					}
				}
				if (rsTeachStudent != null) rsTeachStudent.close();
				if (psTeachStudent != null) psTeachStudent.close();
%>
						   						</select>
											</div>
										</div>
									</th>
								</tr>
<%
    		}
	    	rsTimes.close();
	    	prepStatmTimes.close();
%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
<%
		}
		if (rsTeach != null) rsTeach.close();
		if (prepStatmTeach != null) prepStatmTeach.close();
%>	
		</td>
<%
	}
%>
	</tr>
</table>
</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" name="addnewstuteachavail" class="btn btn-primary btn-lg ">Save</button><br/>
			</div>
		</div>
<%

	if (rsDays != null) rsDays.close();
	if (prepStatmDays != null) prepStatmDays.close();

	myConnection.close();
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.println("DM: "+ex.toString());
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>
	</form>
</div>
<div class="row ajax" style="display:none">
	<div class="col-sm-offset-2 col-md-8">
		<div id="status"></div>
	</div>
</div>
