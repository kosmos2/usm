 <div class="col-sm-12">
 	<h1 class="blue">Timetable</h1>
 <%
 
	String studentId ="";
	if(request.getParameter("STUDENTID")!=null)
	{
		studentId = request.getParameter("STUDENTID");
	}

%>
 	<div class="">
   		<p><a class='btn btn-lg btn-primary pull-right btn-new' href="student.jsp?STUDENTID=<%=studentId %>">Back</a><br/><br/></p>
   	</div> 	
	<div class="record default_div">
		<form class="form-horizontal" id="addnewstudentdaycourse" action="displaycoursedaysavail.jsp" role="form" method="get">
			<input type="hidden" name="STUDENTID" id="STUDENTID" value="<%=studentId%>" />
			<div class="form-group">
				<label for="Course" class="col-sm-2 control-label">Course</label>
 				<div class="col-sm-10">
<%
	try 
	{
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/USM";
		String username = "postgres";
		String password = "postgres";
		String selectedCourse = "SELECT * FROM \"VALUELIST\" WHERE \"NAME\"='COURSES' and \"VALUELISTID\" " 
				+"in (select \"COURSEID\" FROM \"STUDENTCOURSES\" WHERE \"STUDENTID\"='"+studentId+"')";

		Connection myConnection = null;
		PreparedStatement myPreparedStatement = null;
		ResultSet myResultSet = null;
		Class.forName(driver).newInstance();
		myConnection = DriverManager.getConnection(url,username,password);
		myPreparedStatement = myConnection.prepareStatement(selectedCourse);
		myResultSet = myPreparedStatement.executeQuery();

%>
 		
	   				<select name="COURSE" id="COURSE" class="form-control">
<%	   			
		while(myResultSet.next()) 
		{
%>
						<option value="<%=myResultSet.getString("VALUELISTID")%>"><%=myResultSet.getString("VALUE")%></option>
<%
		}

		String selectDay = "SELECT * FROM \"VALUELIST\" WHERE \"NAME\"='DAYS'";
		myPreparedStatement = myConnection.prepareStatement(selectDay);
		myResultSet = myPreparedStatement.executeQuery();
%>
	  				</select>
					<p class="label label-danger COURSE">Select Course</p>
 				</div>
			</div>		
			<div class="form-group">
				<label for="Day" class="col-sm-2 control-label">Day</label>
 				<div class="col-sm-10">
	   				<select name="DAY" id="DAY" class="form-control" multiple size="7">
<%	   			
		while(myResultSet.next()) 
		{
%>
						<option value="<%=myResultSet.getString("VALUELISTID")%>"><%=myResultSet.getString("VALUE")%></option>
<%
		}		
		myResultSet.close();
		myPreparedStatement.close();		
		myConnection.close();
	}
	catch(ClassNotFoundException e)
	{
		e.printStackTrace();
	}
	catch (SQLException ex)
	{
		out.println("Dejan" + ex.toString());
		out.println("SQL Exception: "+ex.getMessage());
		out.println("SQL State: " + ex.getSQLState());
		out.println("Vendor Error: " + ex.getErrorCode());
	}
%>
	  				</select>
					<p class="label label-danger DAY">Select Day(s)</p>
 				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" name="addnewstudentdaycourse" class="btn btn-primary btn-lg ">Next</button><br/>
 				</div>
			</div>  
		</form>
	</div>
	<div class="row ajax" style="display:none">
		<div class="col-sm-offset-2 col-md-8">
  			<div id="status"></div>
 		</div>
	</div>
</div>