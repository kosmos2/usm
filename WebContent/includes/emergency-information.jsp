<%
	String id ="";

	if(request.getParameter("EMERGENCYID")!=null)
	{ 
		id = request.getParameter("EMERGENCYID");
	}

	String p_name="";
	String p_sname="";
	String p_email="";
	String p_home="";
	String p_work="";
	String p_mobile="";
	String p_rel="";

	if(id!="")
	{
		try {
			String driver = "org.postgresql.Driver";
			String url = "jdbc:postgresql://localhost:5432/USM";
			String username = "postgres";
			String password = "postgres";
			String myQuery = "SELECT * FROM \"EMERGENCIES\" WHERE \"EMERGENCYID\"='"+id+"'";
			
			Connection myConnection = null;
			PreparedStatement myPreparedStatement = null;
			ResultSet myResultSet = null;
			Class.forName(driver).newInstance();
			myConnection = DriverManager.getConnection(url,username,password);
			myPreparedStatement = myConnection.prepareStatement(myQuery);
			myResultSet = myPreparedStatement.executeQuery();
			
			while(myResultSet.next()) {
			 
		    p_name=myResultSet.getString("FIRSTNAME");
		    p_sname=myResultSet.getString("SURNAME");
			p_email=myResultSet.getString("EMAIL");
			p_work=myResultSet.getString("WORKPHONE");
			p_home=myResultSet.getString("HOMEPHONE");
			p_mobile=myResultSet.getString("MOBILEPHONE");
		   	p_rel=myResultSet.getString("RELATIONSHIP");  
	   }

		myResultSet.close();
		myPreparedStatement.close();
		myConnection.close();
	}
	catch(ClassNotFoundException e)
	{
		e.printStackTrace();
	}
	catch (SQLException ex)
	{
		out.print("SQLException: "+ex.getMessage());
		out.print("SQLState: " + ex.getSQLState());
		out.print("VendorError: " + ex.getErrorCode());
	} 
}
%>
<div class="student">
	<div class="col-sm-12 student border btm">
   		<p><span class=""><b><%=p_name%></b></span><span class="text"><b>Emergency Contact Information</b></span></p>
	</div>
	<div class="col-sm-2 col-xs-3"></div>
	<div class="col-sm-10 col-xs-9">
		<div class="panel panel-default">
 			<div class="panel-heading white bg1">Student Emergency Information</div>
			<table class="table">

				<tr><td><span class="blue"><b>Name: </b></span><%=p_name%></td></tr>
				<tr><td><span class="blue"><b>Surname: </b></span><%=p_sname%></td></tr>
				<tr><td><span class="blue"><b>Email: </b></span><%=p_email%></td></tr>
				<tr><td><span class="blue"><b>Home: </b></span><%=p_home%></td></tr>
				<tr><td><span class="blue"><b>Work: </b></span><%=p_work%></td></tr>
				<tr><td><span class="blue"><b>Mobile: </b></span><%=p_mobile%></td></tr>
				<tr><td><span class="blue"><b>Relationship: </b></span><%=p_rel%></td></tr>

			</table>
		</div>
	</div>
</div>