 <div class="col-sm-12">
 	<h1 class="blue">Add Course</h1>
 <%
 	String id ="";
	String studentId ="";
	if(request.getParameter("VALUELISTID")!=null)
	{
		id = request.getParameter("VALUELISTID");
	}
	if(request.getParameter("STUDENTID")!=null)
	{
		studentId = request.getParameter("STUDENTID");
	}
%>
 	<div class="">
   		<p><a class='btn btn-lg btn-primary pull-right btn-new' href="student.jsp?STUDENTID=<%=studentId %>">Back</a><br/><br/></p>
   	</div>
	<div class="record default_div">
		<form class="form-horizontal" id="addnewstudentcourse" role="form" action="ajax/addstudentcourse.jsp" method="get">
 <%
	if(id!="")
	{
		out.print("How did this happen?");
	}
	else
	{
%>
	
			<input type="hidden" name="VALUELISTID" id="VALUELISTID" value="" />
			<input type="hidden" name="STUDENTID" id="STUDENTID" value="<%=studentId%>" />
			<div class="form-group">
				<label for="Course" class="col-sm-2 control-label">Course</label>
 				<div class="col-sm-10">
<%
		try 
		{
			String driver = "org.postgresql.Driver";
			String url = "jdbc:postgresql://localhost:5432/USM";
			String username = "postgres";
			String password = "postgres";
			String allCourses = "SELECT * FROM \"VALUELIST\" WHERE \"NAME\"='COURSES' and \"VALUELISTID\" " 
					+"not in (select \"COURSEID\" FROM \"STUDENTCOURSES\" WHERE \"STUDENTID\"='"+studentId+"')";
	
			Connection myConnection = null;
			PreparedStatement myPreparedStatement = null;
			ResultSet myResultSet = null;
			Class.forName(driver).newInstance();
			myConnection = DriverManager.getConnection(url,username,password);
			myPreparedStatement = myConnection.prepareStatement(allCourses);
			myResultSet = myPreparedStatement.executeQuery();

%>
 		
	   				<select name="COURSE" id="COURSE" class="form-control">
	   					<option value=""></option>
<%	   			
			while(myResultSet.next()) 
			{
%>
	    				<option value="<%=myResultSet.getString("VALUELISTID")%>"><%=myResultSet.getString("VALUE")%></option>
<%
			}
			myResultSet.close();
			myPreparedStatement.close();
			myConnection.close();
		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SQLException ex)
		{
			out.print("SQLException: "+ex.getMessage());
			out.print("SQLState: " + ex.getSQLState());
			out.print("VendorError: " + ex.getErrorCode());
		}
%>
	  				</select>
					<p class="label label-danger COURSE">Enter Course</p>
 				</div>
			</div>
<%
	}
%>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" name="addnewstudentcourse" class="btn btn-primary btn-lg ">Submit</button><br/>
 				</div>
			</div>  
		</form>
	</div>
	<div class="row ajax" style="display:none">
		<div class="col-sm-offset-2 col-md-8">
  			<div id="status"></div>
 		</div>
	</div>
</div>