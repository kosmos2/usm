<%@ page contentType="text/html; charset=iso-8859-1" language="java" import="java.sql.* " %>
<%@ page import="java.io.*" %>

<!DOCTYPE html>
<html lang="en"><head>
    
    <title>Music Management System</title>
    <script src="js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cuprum">
    <link href="font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link href="font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css">
   <script type="application/javascript">
$(document).ready(function()
{
$("#searchinput").keyup(function() 
{
var searchbox = $(this).val();
var dataString = 'searchword='+ searchbox;

if(searchbox.length>0)
{

$.ajax({
type: "POST",
url: "ajax/search.jsp",
data: dataString,
cache: false,
success: function(html)
{
$("#display").html(html).show();
}
});
}return false; 
});

$("#display").mouseup(function() 
{
return false
});

$(document).mouseup(function()
{
$('#display').hide();
$('#searchinput').val("");
});
})
</script>
  
    <!--for tooltip-->
    <script>
    
$(document).ready(function(){
   
$("#tooltip-ex a").tooltip({
       
placement : 'top'
    
});
});
$(document).ready(function(){
  
  $("#tooltip-ex1 a").tooltip({
        placement : 'bottom',
    
});
});
</script>
<style>
.Bootstrap-demo
{
   
margin:100px 30px 30px 30px;
}
</style>

  </head>
  
  <body>
     <div class="container">
     
<div class="col-sm-12 student">
  
<nav class="navbar navbar-default bg1 navbar-fixed-top" id="mainNav" >
  
<div class="container">
    
<!-- Brand and toggle get grouped for better mobile display -->
   
<div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="index.jsp" class="navbar-brand white">Universal System</a>
    </div>
    
    
    <!-- Collection of nav links, forms, and other content for toggling -->
    <div id="navbarCollapse" class="collapse navbar-collapse">
      
        <form role="search" class="navbar-form navbar-left" action="#" id="searchbox">
            <div class="form-group">
                <input type="text" name="q" placeholder="Search" id="searchinput" class="form-control">
            </div>
            <div id="display" style="display:none"></div>
        </form>
<ul class="nav navbar-nav">
            <li><a href="displaystudents.jsp" class="white">Students</a></li>
            <li><a href="displayteachers.jsp" class="white">Teachers</a></li>
            <li><a href="displaycourses.jsp" class="white">Courses</a></li>
            <li><a href="displayreports.jsp" class="white">Reports</a></li>
            
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="index.jsp"  class="white">Dashboard</a></li>
        </ul>
        
        
    </div></div>
</nav>
</div>
</div>