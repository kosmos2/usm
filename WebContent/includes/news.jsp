<div class="student">
	<div class="top">
		<a class='btn btn-lg btn-primary pull-right btn-new col-sm-offset-11 col-xs-offset-8' href="addnewstudentcourse.jsp?STUDENTID=<%=id %>">&nbsp;Assign Courses&nbsp;</a><br/>
	</div>
	<br/>
	<br/>
	<div>
		<a class='btn btn-lg btn-primary pull-right btn-new col-sm-offset-11 col-xs-offset-8' href="addstudenttimetable.jsp?STUDENTID=<%=id %>">Assign Timetable</a><br/>
	</div>
	<br/>
	<br/>
<%
String sid ="";

if(request.getParameter("STUDENTID")!=null)
{
	sid = request.getParameter("STUDENTID");
}
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";

	String coursesQuery = "SELECT v.\"VALUE\", sc.\"STUDENTCOURSEID\" FROM \"STUDENTCOURSES\" sc, \"VALUELIST\" v WHERE sc.\"STUDENTID\"='"+
		sid+"' and v.\"VALUELISTID\"=sc.\"COURSEID\"";
	
	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	
	myPreparedStatement = myConnection.prepareStatement(coursesQuery);
	myResultSet = myPreparedStatement.executeQuery();
%>
		<div class="panel panel-default">
			<div class="panel-heading white bg1"><span class="white"> Courses attending</span></div>
 			<table class="table">
<%
		while(myResultSet.next())
		{
%>
  				<tr>
  					<td><span class="blue"><b>Course Name: </b></span><%=myResultSet.getString("VALUE")%></td>
  					<td class='deleterow'><a data-url="studentcourse" data-id="<%=myResultSet.getInt("STUDENTCOURSEID") %>" href="javascript:void(0)"><div class='glyphicon glyphicon-remove'></div></a></td>
  				</tr>
<%
		}
%>
 			</table>
		</div>
<%		
		myResultSet.close();
		myPreparedStatement.close();
		myConnection.close();
	}
	catch(ClassNotFoundException e)
	{
		e.printStackTrace();
	}
	catch (SQLException ex)
	{
		out.print("SQLException: "+ex.getMessage());
		out.print("SQLState: " + ex.getSQLState());
		out.print("VendorError: " + ex.getErrorCode());
	}
%>
<%
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";

	String timetableQuery = "SELECT ts.\"DAY\", ts.\"TIME\", t.\"FIRSTNAME\", t.\"SURNAME\", ts.\"TEACHERSTUDENTID\", ts.\"COURSEID\"" +
			"FROM \"TEACHERSTUDENTS\" ts, \"TEACHERS\" t WHERE ts.\"STUDENTID\"='"+sid+"' and t.\"TEACHERID\"=ts.\"TEACHERID\"";
	
	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	
	myPreparedStatement = myConnection.prepareStatement(timetableQuery);
	myResultSet = myPreparedStatement.executeQuery();
%>
		<div class="panel panel-default">
			<div class="panel-heading white bg1"><span class="white"> Timetable</span></div>
 			<table class="table">
<%
		while(myResultSet.next())
		{
			
			String myCoursesQuery = "SELECT * FROM \"VALUELIST\" WHERE \"NAME\"='COURSES' and \"VALUELISTID\"="+myResultSet.getInt("COURSEID")+";";
			
			PreparedStatement myCoursesStatement = null;
			ResultSet myCoursesSet = null;
			
			myCoursesStatement = myConnection.prepareStatement(myCoursesQuery);
			myCoursesSet = myCoursesStatement.executeQuery();
%>
  				<tr>
  					<td><span class="blue"><b><%=myResultSet.getString("FIRSTNAME")%>, <%=myResultSet.getString("SURNAME")%></b></span></td>
  					<td><%=myResultSet.getString("DAY")%></td>
  					<td><%=myResultSet.getString("TIME")%></td>
<%
			while(myCoursesSet.next())
			{
%>
  					<td><%=myCoursesSet.getString("VALUE")%></td>
  					<td class='deleterow'><a data-url="teacherstudent" data-id="<%=myResultSet.getInt("TEACHERSTUDENTID") %>" href="javascript:void(0)"><div class='glyphicon glyphicon-remove'></div></a></td>
  				</tr>
<%
			}
			myCoursesSet.close();
			myCoursesStatement.close();
		}
%>
 			</table>
		</div>
<%		
		myResultSet.close();
		myPreparedStatement.close();
		myConnection.close();
	}
	catch(ClassNotFoundException e)
	{
		e.printStackTrace();
	}
	catch (SQLException ex)
	{
		out.print("SQLException: "+ex.getMessage());
		out.print("SQLState: " + ex.getSQLState());
		out.print("VendorError: " + ex.getErrorCode());
	}
%>
</div>