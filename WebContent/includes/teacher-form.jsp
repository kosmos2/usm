<div class="col-sm-12">
	<h1 class="blue">Add Teacher </h1>
	<div class="">
   		<p><a class='btn btn-lg btn-primary addnewrow pull-right btn-new' href="displayteachers.jsp">View Teachers </a><br/><br/></p>
   	</div>
  	<div class="record default_div">
		<form class="form-horizontal" id="addteacher" role="form" action="ajax/addteacher.jsp" method="get">
<%
String id ="";
  
if(request.getParameter("TEACHERID")!=null)
{
	id = request.getParameter("TEACHERID");
}

if(id!="")
{
	try {
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/USM";
		String username = "postgres";
		String password = "postgres";
		String myDataField = "Dejan";
		String myQuery = "SELECT * FROM \"TEACHERS\" WHERE \"TEACHERID\"='"+id+"'";
		
		Connection myConnection = null;
		PreparedStatement myPreparedStatement = null;
		ResultSet myResultSet = null;
		Class.forName(driver).newInstance();
		myConnection = DriverManager.getConnection(url,username,password);
		myPreparedStatement = myConnection.prepareStatement(myQuery);
		myResultSet = myPreparedStatement.executeQuery();
		myResultSet.next();

%>

			<input type="hidden" name="TEACHERID" id="TEACHERID" value="<%=myResultSet.getInt("TEACHERID")%>" />
			<div class="form-group">
				<label for="firstname" class="col-sm-2 control-label">First Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="FIRSTNAME"
							placeholder="Enter First Name" maxlength="50" name="FIRSTNAME" value="<%=myResultSet.getString("FIRSTNAME")%>">
							<p class="label label-danger FIRSTNAME">Enter First Name</p>
					</div>
			</div>
			<div class="form-group">
				<label for="lastname" class="col-sm-2 control-label">Surname</label>
				<div class="col-sm-10">
					<input type="text" class="form-control"  maxlength="50" value="<%=myResultSet.getString("SURNAME")%>" name="SURNAME" id="SURNAME"
							placeholder="Enter Surname">
					<p class="label label-danger SURNAME">Enter Surname </p>
				</div>
			</div>

<%	
  
		myResultSet.close();
		myPreparedStatement.close();
		myConnection.close();
	}
	catch(ClassNotFoundException e)
	{
		e.printStackTrace();
	}
}
else
{
%>
    		<input type="hidden" name="TEACHERID" id="TEACHERID" value="" />
 			<div class="form-group">
 				<label for="firstname" class="col-sm-2 control-label">First Name</label>
 				<div class="col-sm-10">
 					<input type="text" class="form-control" id="FIRSTNAME"
 							placeholder="Enter First Name" maxlength="50" name="FIRSTNAME" value="">
 					<p class="label label-danger FIRSTNAME">Enter First Name</p>
 				</div>
 			</div>
 			<div class="form-group">
 				<label for="lastname" class="col-sm-2 control-label">Surname</label>
 				<div class="col-sm-10">
 					<input type="text" class="form-control"  maxlength="50" value="" name="SURNAME" id="SURNAME"
 							placeholder="Enter Surname">
 					<p class="label label-danger SURNAME">Enter Surname </p>
 				</div>
 			</div>

<%
}
%>
			<div class="form-group">
 				<div class="col-sm-offset-2 col-sm-10">
 					<button type="submit" name="addteacher" class="btn btn-primary btn-lg ">
 						Save</button><br/>
 				</div>
 			</div>
		</form>
	</div>
	<div class="row ajax" style="display:none">
		<div class="col-sm-offset-2 col-md-8">
			<div id="status"></div>
		</div>
	</div>
</div>