<div class="top1">
   <p><button class='btn btn-lg btn-primary addnewrow pull-right btn-new'>Add New <span class="glyphicon glyphicon-plus"></span></button><br/><br/></p>
</div>
 
<div class="table-responsive top">

	<table class="table" id="myTable">
 		<thead>
 			<tr>
 				<th>View More</th>
 				<th>Student Name</th>
 				<th>Surname</th>
 				<th class="hide">Street</th>
 				<th class="hide">Suburb</th>
 				<th class="hide">Postal code</th>
 				<th class="hide">State</th>
 				<th>Email</th>
 				<th>Mobile Phone</th>
 				<th class="hide"><span class="glyphicon glyphicon-phone-alt left"></span></th>
 				<th class="hide"><span class="glyphicon glyphicon-earphone left"></span></th>
 				<th class="hide"><i class="fa fa-mobile icon"></i></th>
  				<th>Edit</th>
   				<th>Delete</th>
 			</tr>
 		</thead>
 		<tbody>
  <%
	try 
  	{
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/USM";
		String username = "postgres";
		String password = "postgres";
	
		String myQuery = "SELECT * FROM \"STUDENTS\"";
	
		Connection myConnection = null;
		PreparedStatement myPreparedStatement = null;
		ResultSet myResultSet = null;
		Class.forName(driver).newInstance();
		myConnection = DriverManager.getConnection(url,username,password);
		myPreparedStatement = myConnection.prepareStatement(myQuery);
		myResultSet = myPreparedStatement.executeQuery();
	    while(myResultSet.next())
	    {
	
%>

 			<tr class="tab-row tab-row-<%=myResultSet.getInt("STUDENTID") %>">
				<td class=""><a href="student.jsp?STUDENTID=<%=myResultSet.getInt("STUDENTID") %>" data-id="<%=myResultSet.getInt("STUDENTID") %>" class="btn btn-success">View More</a></td>
			   	<td><%=myResultSet.getString("FIRSTNAME") %></td>
			   	<td><%=myResultSet.getString("SURNAME") %></td>
			   	<td class="hide"><%=myResultSet.getString("STREET") %></td>
			   	<td class="hide"><%=myResultSet.getString("SUBURB") %></td>
			    <td class="hide"><%=myResultSet.getString("POSTCODE") %></td> 
			    <td class="hide"><%=myResultSet.getString("STATE") %></td>
			   	<td><%=myResultSet.getString("EMAIL") %></td>
			 	<td class="hide"><%=myResultSet.getString("DOB") %></td>
			   	<td class="hide"><%=myResultSet.getString("HOMEPHONE") %></td>
			   	<td class="hide"><%=myResultSet.getString("WORKPHONE") %></td> 
			    <td><%=myResultSet.getString("MOBILEPHONE") %></td> 
			  	<td><a href="addstudent.jsp?STUDENTID=<%=myResultSet.getInt("STUDENTID") %>"><div class='glyphicon glyphicon-pencil'></div></a></td>
			 	<td class='deleterow'><a data-url="student" data-id="<%=myResultSet.getInt("STUDENTID") %>" href="javascript:void(0)"><div class='glyphicon glyphicon-remove'></div></a></td>
			</tr>
 
<% 
		}

		myResultSet.close();
		myPreparedStatement.close();
		myConnection.close();
	}
	catch(ClassNotFoundException e)
	{
		e.printStackTrace();
	}
	catch (SQLException ex)
	{
		out.print("SQLException: "+ex.getMessage());
		out.print("SQLState: " + ex.getSQLState());
		out.print("VendorError: " + ex.getErrorCode());
	}
%>
 		</tbody>
 	</table>
</div>	