<%
String id ="";

if(request.getParameter("STUDENTID")!=null)
{
	id = request.getParameter("STUDENTID");
}

if (id != "")
{
%>
<div class="top1">
   <p><a href="addparent.jsp?STUDENTID=<%=id%>"><button class='btn btn-lg btn-primary pull-right btn-new'>Add New <span class="glyphicon glyphicon-plus"></span></button></a><br/><br/></p>
</div>
<%
}
%>

<div class="table-responsive top1">
	<table class="table table-striped">
		<thead>
			<tr>
 				<th>View More</th>
				<th>First Name</th>
				<th>Surname</th>
				<th>Email</th>
				<th class="hide"><span class="glyphicon glyphicon-phone-alt left"></span></th>
				<th class="hide"><span class="glyphicon glyphicon-earphone left"></span></th>
				<th class="hide"><i class="fa fa-mobile icon"></i></th>
				<th>Occupation</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
<%
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";
	String myDataField = "Dejan";
	String myQuery = "SELECT * FROM \"PARENTS\" WHERE \"STUDENTID\"="+id+"";
	String ajax = "SELECT * FROM \"PARENTS\"";
	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	if (id.equals(""))
	{
		myPreparedStatement = myConnection.prepareStatement(ajax);
	}
	else
	{
		myPreparedStatement = myConnection.prepareStatement(myQuery);
	}
	myResultSet = myPreparedStatement.executeQuery();
    while(myResultSet.next()) {
%>

			<tr class="tab-row tab-row-<%=myResultSet.getInt("PARENTID") %>">
				<td class=""><a href="parent.jsp?PARENTID=<%=myResultSet.getInt("PARENTID") %>&STUDENTID=<%=id %>"  data-id="<%=myResultSet.getInt("PARENTID") %>" class="btn btn-success">View More</a></td>
				<td><%=myResultSet.getString("FIRSTNAME") %></td>
			   	<td><%=myResultSet.getString("SURNAME") %></td>
			   	<td><%=myResultSet.getString("EMAIL") %></td>
				<td class="hide"><%=myResultSet.getString("HOMEPHONE") %></td>
				<td class="hide"><%=myResultSet.getString("WORKPHONE") %></td> 
				<td class="hide"><%=myResultSet.getString("MOBILEPHONE") %></td> 
				<td><%=myResultSet.getString("OCCUPATION") %></td>
				<td><a href="addparent.jsp?PARENTID=<%=myResultSet.getInt("PARENTID") %>&STUDENTID=<%=id %>"><div class='glyphicon glyphicon-pencil'></div></a></td>
				<td class='deleterow'><a data-url="parent" data-id="<%=myResultSet.getInt("PARENTID") %>" href="javascript:void(0)"><div class='glyphicon glyphicon-remove'></div></a></td>
			</tr>

<% 
	}

	myResultSet.close();
	myPreparedStatement.close();
	myConnection.close();
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>
		</tbody>
	</table>
</div>