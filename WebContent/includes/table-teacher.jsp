<div class="top1">
	<p><button class='btn btn-lg btn-primary addnewrow2 pull-right btn-new'>Add New <span class="glyphicon glyphicon-plus"></span></button><br/><br/></p>
</div>
 
<div class="table-responsive top">
 	<table class="table table-striped" id="myTable">
 		<thead>
		<tr>
			<th>View More</th>
 			<th>First Name</th>
 			<th>Surname</th>
  			<th>Edit</th>
   			<th>Delete</th>
		</tr>
 		</thead>
 		<tbody>
  <%
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";

	String myQuery = "SELECT * FROM \"TEACHERS\"";

	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	myPreparedStatement = myConnection.prepareStatement(myQuery);
	myResultSet = myPreparedStatement.executeQuery();
    while(myResultSet.next()) {
%>

 		<tr class="tab-row tab-row-<%=myResultSet.getInt("TEACHERID") %>">
  			<td class=""><a href="teacher.jsp?TEACHERID=<%=myResultSet.getInt("TEACHERID") %>"  data-id="<%=myResultSet.getInt("TEACHERID") %>" class="btn btn-success">View More</a></td>
			<td><%=myResultSet.getString("FIRSTNAME") %></td>
   			<td ><%=myResultSet.getString("SURNAME") %></td>
			<td><a href="addteacher.jsp?TEACHERID=<%=myResultSet.getInt("TEACHERID") %>"><div class='glyphicon glyphicon-pencil'></div></a></td>
 			<td class='deleterow'><a data-url="teacher" data-id="<%=myResultSet.getInt("TEACHERID") %>" href="javascript:void(0)"><div class='glyphicon glyphicon-remove'></div></a></td>
 		</tr>

<% 
	} 

	myResultSet.close();
	myPreparedStatement.close();
	myConnection.close();
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>
		</tbody>
	</table>
</div>