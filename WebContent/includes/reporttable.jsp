<div class="top1">
<!--    <p><button class='btn btn-lg btn-primary addnewrow pull-right btn-new'>Add New <span class="glyphicon glyphicon-plus"></span></button><br/><br/></p> -->
<a href="report" target="_blank"'>List of Emergency Contacts Report</a>
</div>
 
<div class="table-responsive top1">
  
 
 <table class="table table-striped">
 <thead>
 <tr>
 <th>Student  First Name</th>
 <th>Student  Surname</th>
 <th>Student  Email</th>
 <th>Student  Mobile Phone</th>
 <th>Parent First Name</th>
 <th>Parent Occupation</th>
 <th>Parent Email</th>
 <th>Parent Mobile Phone</th> 
 <th>Emergency  First Name</th>
 <th>Emergency  Mobile Phone</th>
 <th>Emergency  Relationship</th>
 
 
<!--   <th class="hide"><span class="glyphicon glyphicon-phone-alt left"></span></th> -->
<!--  <th class="hide"><span class="glyphicon glyphicon-earphone left"></span></th> -->
<!--  <th class="hide"><i class="fa fa-mobile icon"></i></th> -->
<!--   <th>Edit</th> -->
<!--    <th>Delete</th> -->
 </tr>
 </thead>
 <tbody>
 
  <%
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";
	
 	String myQuery = "SELECT e.\"FIRSTNAME\" emergencyfirstname,e.\"MOBILEPHONE\" emergencymobilephone, e.\"RELATIONSHIP\" emergencyrelationship,p.\"FIRSTNAME\" parentfirstname,p.\"OCCUPATION\" parentoccupation, p.\"MOBILEPHONE\" parentmobilephone,p.\"EMAIL\" parentemail, s.\"FIRSTNAME\" studentfirstname,s.\"SURNAME\" studentsurname,s.\"EMAIL\" studentemail, s.\"MOBILEPHONE\" studentmobilephone FROM \"EMERGENCIES\" e, \"PARENTS\" p, \"STUDENTS\" s WHERE p.\"STUDENTID\" = e.\"STUDENTID\" AND s.\"STUDENTID\" = p.\"STUDENTID\"";
	
	Connection myConnection = null;
	PreparedStatement myPreparedStatement = null;
	ResultSet myResultSet = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	myPreparedStatement = myConnection.prepareStatement(myQuery);
	
    myResultSet = myPreparedStatement.executeQuery();
    
    %>
		
			<table class="table table-striped">
<%
	while(myResultSet.next())
	{
%>
				<tr>
					<td><%=myResultSet.getString("studentfirstname")%></td>
					<td><%=myResultSet.getString("studentsurname")%></td>
					<td><%=myResultSet.getString("studentemail")%></td>
					<td><%=myResultSet.getString("studentmobilephone")%></td>
					
					<td><%=myResultSet.getString("parentfirstname")%></td>
					<td><%=myResultSet.getString("parentoccupation")%></td>
					<td><%=myResultSet.getString("parentemail")%></td>
					<td><%=myResultSet.getString("parentmobilephone")%></td>
					
					<td><%=myResultSet.getString("emergencyfirstname")%></td>
					<td><%=myResultSet.getString("emergencymobilephone")%></td>
					<td><%=myResultSet.getString("emergencyrelationship")%></td>
					
				</tr>
<%
	}
%>
			</table>
	</div>
<%		
	myResultSet.close();
	myPreparedStatement.close();
	myConnection.close();
}

    
    
catch(ClassNotFoundException e)
	{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>
 </tbody>
 </table>
</div>	