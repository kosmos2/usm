<%
String id ="";
  
if(request.getParameter("TEACHERID")!=null)
{
	id = request.getParameter("TEACHERID");
}
String t_name="";
String t_sname="";
int t_id=0;

if(id!="")
{
	try 
	{
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/USM";
		String username = "postgres";
		String password = "postgres";
		String myQuery = "SELECT * FROM \"TEACHERS\" WHERE \"TEACHERID\"='"+id+"'";
		
		Connection myConnection = null;
		PreparedStatement myPreparedStatement = null;
		ResultSet rsTeacher = null;
		Class.forName(driver).newInstance();
		myConnection = DriverManager.getConnection(url,username,password);
		myPreparedStatement = myConnection.prepareStatement(myQuery);
		rsTeacher = myPreparedStatement.executeQuery();
	
		while(rsTeacher.next()) 
		{
	    	t_name=rsTeacher.getString("FIRSTNAME");
	    	t_sname=rsTeacher.getString("SURNAME");
	    	t_id=rsTeacher.getInt("TEACHERID");
   		}

		rsTeacher.close();
		myPreparedStatement.close();
		myConnection.close();
	}
	catch(ClassNotFoundException e)
	{
		e.printStackTrace();
	}
	catch (SQLException ex)
	{
		out.print("SQLException: "+ex.getMessage());
		out.print("SQLState: " + ex.getSQLState());
		out.print("VendorError: " + ex.getErrorCode());
	} 
}
%>
<div class="student">
	<div class="col-sm-12 col-xs-9">
		<div class="panel panel-default">
 			<div class="panel-heading white bg1"><span class="white"><%=t_name%>&nbsp;<%=t_sname%></span></div>
			<div class="top">
				<a class='btn btn-lg btn-primary addnewrow pull-right btn-new' href="addnewteachercourse.jsp?TEACHERID=<%=id %>">Assign Course</a><br/>
			</div> 			
   			<div class="table">
 				<table class="table">
			 		<thead>
						<tr>
			 				<th>Course</th>
			   				<th>Description</th>
			   				<th>Remove</th>
						</tr>
			 		</thead>
			 		<tbody>
  <%
try {
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";
	
	String teacherCourseQuery = "SELECT * FROM \"TEACHERSKILLS\" where \"TEACHERID\"="+t_id+"";
	
	Connection myConnection = null;
	PreparedStatement prepStatmTeacherCourse = null;
	PreparedStatement prepStatmCourseValuelist = null;
	ResultSet rsTeacherCourses = null;
	ResultSet rsCourses = null;
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	prepStatmTeacherCourse = myConnection.prepareStatement(teacherCourseQuery);

	rsTeacherCourses = prepStatmTeacherCourse.executeQuery();
	
    while(rsTeacherCourses.next()) 
    {
%>
 		<tr class="tab-row tab-row-<%=t_id %>">
<%
		String coursesQuery = "SELECT * FROM \"VALUELIST\" where \"NAME\"='COURSES' and \"VALUELISTID\" = "+rsTeacherCourses.getInt("VALUELISTID");
		prepStatmCourseValuelist = myConnection.prepareStatement(coursesQuery);
		rsCourses = prepStatmCourseValuelist.executeQuery();
		
		while(rsCourses.next())
		{
%>
			<td><%=rsCourses.getString("VALUE") %></td>
			<td><%=rsCourses.getString("DESCRIPTION") %></td>
 			<td class='deleterow'><a data-url="teachercourse" data-id="<%=rsTeacherCourses.getInt("TEACHERSKILLID") %>" href="javascript:void(0)"><div class='glyphicon glyphicon-remove'></div></a></td>
<%
		}
%>
 		</tr>

<% 
	} 

	rsTeacherCourses.close();
	prepStatmTeacherCourse.close();
	if (rsCourses != null)
		rsCourses.close();
	if (prepStatmCourseValuelist != null)
		prepStatmCourseValuelist.close();	
	myConnection.close();
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>
					</tbody>
				</table>
			</div>
		</div>
		<div class="panel panel-default">
 			<div class="panel-heading white bg1"><span class="white">Teacher Availability</span></div>
			<div class="top">
				<a class='btn btn-lg btn-primary addnewrow pull-right btn-new' href="displayteachersavailability.jsp?TEACHERID=<%=id %>">Set Teacher Availability</a><br/>
			</div> 	 			
 			<div class="record default_div">
   				<div class="table">
 					<table class="table">
			 			<thead>
							<tr>
								<th></th>
				 				<th>MONDAY</th>
				   				<th>TUESDAY</th>
				   				<th>WEDNESDAY</th>
				   				<th>THURSDAY</th>
								<th>FRIDAY</th>
				   				<th>SATURDAY</th>
				   				<th>SUNDAY</th>
							</tr>
			 			</thead>
			 			<tbody>
  <%
try 
{
	String driver = "org.postgresql.Driver";
	String url = "jdbc:postgresql://localhost:5432/USM";
	String username = "postgres";
	String password = "postgres";
	
	String timesQuery = "SELECT * FROM \"VALUELIST\" where \"NAME\"='TIMES'";
	String daysQuery = "SELECT * FROM \"VALUELIST\" where \"NAME\"='DAYS'";

	Connection myConnection = null;
	
	PreparedStatement prepStatmTeacherAv = null;
	PreparedStatement prepStatmDayValuelist = null;
	PreparedStatement prepStatmTimeValuelist = null;
	
	ResultSet rsTeacherAv = null;
	ResultSet rsDays = null;
	ResultSet rsTimes = null;
	
	Class.forName(driver).newInstance();
	myConnection = DriverManager.getConnection(url,username,password);
	
	prepStatmDayValuelist = myConnection.prepareStatement(daysQuery);
	rsDays = prepStatmDayValuelist.executeQuery();
	
	prepStatmTimeValuelist = myConnection.prepareStatement(timesQuery);
	rsTimes = prepStatmTimeValuelist.executeQuery();	
	
    while(rsTimes.next())
    {
%>
 							<tr class="tab-row tab-row-<%=t_id %>">
 								<td><%=rsTimes.getString("VALUE") %></td>
<% 
		rsDays = prepStatmDayValuelist.executeQuery();
		while(rsDays.next())
		{
			String teacherAvQuery = "SELECT * FROM \"TEACHERAVAILABILITY\" where \"TEACHERID\"='"+t_id+"' and \"DAY\"='"+rsDays.getString("VALUE")+"' and \"TIME\"='"+rsTimes.getString("VALUE")+"'";
			prepStatmTeacherAv = myConnection.prepareStatement(teacherAvQuery);
			rsTeacherAv = prepStatmTeacherAv.executeQuery();
			
			if (rsTeacherAv.next()) 
			{
				String studentQuery = "SELECT s.\"FIRSTNAME\",s.\"SURNAME\" FROM \"TEACHERSTUDENTS\" ts, \"STUDENTS\" s where \"TEACHERID\"='"+t_id+
						"' and \"DAY\"='"+rsDays.getString("VALUE")+"' and \"TIME\"='"+rsTimes.getString("VALUE")+
						"' and s.\"STUDENTID\"=ts.\"STUDENTID\"";
				PreparedStatement prepStatmStudent = myConnection.prepareStatement(studentQuery);
				ResultSet rsStudent = prepStatmStudent.executeQuery();
%>
								<td bgcolor="#CCFFCC">
<%
				String inClass = "";
				while (rsStudent.next()) 
				{
					inClass = inClass + rsStudent.getString("FIRSTNAME") + " " + rsStudent.getString("SURNAME") + ",";
				}
				if (!(inClass.equals("")))
				{
					inClass = inClass.substring(0, inClass.length()-1);
				}
%>
									<%=inClass %>
								</td>
<%
				if (rsStudent != null) rsStudent.close();
				if (prepStatmStudent != null) prepStatmStudent.close();
			}
			else
			{
%>
								<td bgcolor="#FF9933"></td>
<%
			}
		}
%>
 							</tr>
<% 
	} 
	if (rsTeacherAv != null) rsTeacherAv.close();
	if (prepStatmTeacherAv != null) prepStatmTeacherAv.close();
    rsDays.close();
    prepStatmDayValuelist.close();
    rsTimes.close();
    prepStatmTimeValuelist.close();    
	myConnection.close();
}
catch(ClassNotFoundException e)
{
	e.printStackTrace();
}
catch (SQLException ex)
{
	out.print("SQLException: "+ex.getMessage());
	out.print("SQLState: " + ex.getSQLState());
	out.print("VendorError: " + ex.getErrorCode());
}
%>
						</tbody>
					</table>
				</div>
			</div>
		</div>		
	</div>
</div>