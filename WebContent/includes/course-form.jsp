<div class="col-sm-12">
	<h1 class="blue">Add Course </h1>
	<div class="">
		<p><a class='btn btn-lg btn-primary addnewrow pull-right btn-new' href="displaycourses.jsp">View Courses </a><br/><br/></p>
   	</div>
  	<div class="record default_div">
		<form class="form-horizontal" id="addcourse" role="form" action="ajax/addcourse.jsp" method="get">
 <%
String id ="";
  
if(request.getParameter("VALUELISTID")!=null)
{
	id = request.getParameter("VALUELISTID");
}

if(id!="")
{
	try {
		String driver = "org.postgresql.Driver";
		String url = "jdbc:postgresql://localhost:5432/USM";
		String username = "postgres";
		String password = "postgres";
		String myQuery = "SELECT * FROM \"VALUELIST\" WHERE \"VALUELISTID\"='"+id+"'";
		
		Connection myConnection = null;
		PreparedStatement myPreparedStatement = null;
		ResultSet myResultSet = null;
		Class.forName(driver).newInstance();
		myConnection = DriverManager.getConnection(url,username,password);
		myPreparedStatement = myConnection.prepareStatement(myQuery);
		myResultSet = myPreparedStatement.executeQuery();
		myResultSet.next();

%>

			<input type="hidden" name="VALUELISTID" id="VALUELISTID" value="<%=myResultSet.getInt("VALUELISTID")%>" />

 			<div class="form-group">
 				<label for="value" class="col-sm-2 control-label">Course Name</label>
 				<div class="col-sm-10">
 					<input type="text" class="form-control" id="VALUE"
 						placeholder="Enter First Name" maxlength="100" name="VALUE" value="<%=myResultSet.getString("VALUE")%>">
 					<p class="label label-danger VALUE">Enter Course Name</p>
 				</div>
 			</div>
 			<div class="form-group">
 				<label for="Description" class="col-sm-2 control-label">Description</label>
 				<div class="col-sm-10">
 					<input type="text" class="form-control"  maxlength="250" value="<%=myResultSet.getString("DESCRIPTION")%>" name="DESCRIPTION" id="DESCRIPTION"
 						placeholder="Enter Description">
 					<p class="label label-danger DESCRIPTION">Enter Course Description </p>
 				</div>
			</div>
<%	
  
		myResultSet.close();
		myPreparedStatement.close();
		myConnection.close();
	}
	catch(ClassNotFoundException e)
	{
		e.printStackTrace();
	}

}
else
{
%>
	
    		<input type="hidden" name="VALUELISTID" id="VALUELISTID" value="" />
 			<div class="form-group">
 				<label for="coursename" class="col-sm-2 control-label">Course Name</label>
 				<div class="col-sm-10">
 					<input type="text" class="form-control" id="VALUE"
 						placeholder="Enter Course Name" maxlength="100" name="VALUE" value="">
 					<p class="label label-danger COURSENAME">Enter Course Name</p>
 				</div>
 			</div>
 			<div class="form-group">
 				<label for="description" class="col-sm-2 control-label">Description</label>
 				<div class="col-sm-10">
 					<input type="text" class="form-control"  maxlength="250" value="" name="DESCRIPTION" id="DESCRIPTION"
 						placeholder="Enter Description">
 					<p class="label label-danger DESCRIPTION">Enter Description </p>
 				</div>
			</div>
<%
}
%>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" name="addcourse" class="btn btn-primary btn-lg ">Save</button><br/>
 				</div>
			</div>
		</form>
	</div>
	<div class="row ajax" style="display:none">
		<div class="col-sm-offset-2 col-md-8">
  			<div id="status"></div>
		</div>
	</div>
</div>